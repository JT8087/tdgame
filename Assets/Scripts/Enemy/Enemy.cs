﻿using UnityEngine;
using System.Collections;
using TDGame.ObjectPooling;

namespace TDGame.Enemys
{
	public abstract class Enemy : MonoBehaviour, IPoolableObject
	{
		[SerializeField]protected float m_Health = 100.0f;
		[SerializeField]protected float m_Speed = 10.0f;
		[SerializeField]protected int m_TargetWaypoint = 0;
		[SerializeField]protected int m_Damage = 10;
		[SerializeField]protected int m_Reward = 10;

		public Vector3 m_HitOffset = Vector3.zero;

		protected ObjectPool pool;
		public int id;

		abstract public void gotHit(float damage);

		abstract protected void nextWayPoint();
		abstract protected void reachEnd();
		abstract protected void move();
		abstract public bool isDead();
		abstract public void recycle();
		abstract public bool isIdle();
		abstract public void reset();

		virtual public void register(ObjectPool pool, int id) {
			this.pool = pool;
			this.id = id;
		}
	}
}
