﻿using UnityEngine;
using System.Collections;
using TDGame.TimerManagement;

// Timer: Use to count down a certain time.
public class Timer : Object{
	int _ID;
	bool _bActivated;
	bool _bCountingTime;
	bool _bRepeatable;
	bool _bFixed;
	float _fTime;
	float _fCurrentTime;
	float _fIdleTime;

	public Timer() {
		_ID = -1;
		_bActivated = false;
		_bCountingTime = false;
		_bRepeatable = false;
		_bFixed = false;
		_fTime = 0;
		_fCurrentTime = 0;
		_fIdleTime = 0;
	}

	public Timer(bool repeatable) : this() {
		_bRepeatable = repeatable;
	}

	public Timer(float time) : this() {
		_fTime = time;
	}

	public Timer(float time, bool repeatable) : this() {
		_bRepeatable = repeatable;
		_fTime = time;
	}

	/// <summary>
	/// Gets how many times this timer repeated.
	/// Be noted, if the timer doesn't set to repeatable, this will make it auto restart timing after it reached the time.
	/// </summary>
	/// <returns>How many times has repeated after last check.</returns>
	public int GetRepeatedTimes() {
		return GetRepeatedTimes(_fTime);
	}

	/// <summary>
	/// Get how many times this timer repeated.
	/// Be noted, if the timer doesn't set to repeatable, this will do nothing and return -1.
	/// </summary>
	/// <returns>How many times has repeated after last check.</returns>
	/// <param name="time">Time.</param>
	public int GetRepeatedTimes(float time) {
		resetIdle();
		if (time <= 0) {
			Debug.LogWarning("Counting time is zero!!!");
		}
		if (!_bRepeatable) return -1;
		if (!_bActivated) {
			StartTimer(time);
		}

		if (_fCurrentTime >= time) {
			int times = (int)(_fCurrentTime / time);
			_fCurrentTime -= time * times;
			return times;
		}
		return 0;
	}

	/// <summary>
	/// Clears passed time.
	/// </summary>
	public void ClearPassedTime() {
		_fCurrentTime = 0;
		resetIdle();
	}

	/// <summary>
	/// Pause and reset timer to a new certain time.
	/// </summary>
	public void ResetTimer() {
		ClearPassedTime();
		PauseTimer();
		resetIdle();
	}

	/// <summary>
	/// Stops and resets timer to a new certain time.
	/// </summary>
	public void ResetTimer(float time) {
		_fTime = time;
		ResetTimer();
	}

	/// <summary>
	/// Starts the timer.
	/// </summary>
	public void StartTimer() {
		Login();
		ClearPassedTime();
		_bActivated= true;
		_bCountingTime = true;
		resetIdle();
	}

	/// <summary>
	/// Starts the timer and make it counting certain time.
	/// </summary>
	public void StartTimer(float time) {
		_fIdleTime = time;
		StartTimer();
	} 

	/// <summary>
	/// Stops the timer.
	/// </summary>
	public void StopTimer() {
		Logout();
		_bActivated = false;
		_bCountingTime = false;
		resetIdle();
	}

	/// <summary>
	/// Pauses the timer.
	/// </summary>
	public void PauseTimer() {
		_bCountingTime = false;
		resetIdle();
	}

	/// <summary>
	/// Unpauses the timer.
	/// </summary>
	public void UnpauseTimer() {
		_bCountingTime = true;
		resetIdle();
	}

	/// <summary>
	/// Gets or sets a value indicating whether this <see cref="Timer"/> is fixed.
	/// If it isn't fixed, it will stop itself when you not check on it after finish countdown 3 times.
	/// Useful when you can't tell when will this reference stop being use.
	/// For example, when a enemy that use timers being destory, those timers it used will stop and free themselves after some times.
	/// If it is fixed, it won't be automatically stop itself.
	/// Useful when you want to pause a timer for a long time but not use it.
	/// </summary>
	/// <value><c>true</c> if is fixed; otherwise, <c>false</c>.</value>
	public bool isFixed {
		get { return _bFixed; }
		set { _bFixed = value;}
	}

	/// <summary>
	/// Gets a value indicating whether this <see cref="Timer"/> is timing.
	/// </summary>
	/// <value><c>true</c> if is timing; otherwise, <c>false</c>.</value>
	public bool isTiming {
		get { 
			resetIdle();
			return _bCountingTime; 
		}
	}

	/// <summary>
	/// Gets a value indicating whether this <see cref="Timer"/> is activated.
	/// </summary>
	/// <value><c>true</c> if is activated; otherwise, <c>false</c>.</value>
	public bool isActivated {
		get { 
			resetIdle();
			return _bActivated; 
		}
	}

	/// <summary>
	/// Gets a value indicating whether this <see cref="Timer"/> is done timing.
	/// </summary>
	/// <value><c>true</c> if is done timing; otherwise, <c>false</c>.</value>
	public bool isTimingDone {
		get { 
			resetIdle();
			return _fCurrentTime >= _fTime; 
		}
	}

	/// <summary>
	/// Gets or sets a value indicating whether this <see cref="Timer"/> is repeatable.
	/// </summary>
	/// <value><c>true</c> if is repeatable; otherwise, <c>false</c>.</value>
	public bool isRepeatable {
		get {
			resetIdle();
			return _bRepeatable;
		}

		set {
			resetIdle();
			_bRepeatable = value;
		}
	}

	/// <summary>
	/// Gets how much time has passed.
	/// </summary>
	/// <value>The passed time.</value>
	public float PassedTime {
		get {
			resetIdle();
			return _fCurrentTime;
		}
	}

	/// <summary>
	/// Gets how much time has passed in percentage.
	/// </summary>
	/// <value>The passed percent.</value>
	public float PassedPercent {
		get {
			resetIdle();
			return _fCurrentTime / _fTime;
		}
	}

	void Login() {
		if (_ID >= 0) return;
		_ID = TimerManager.Instance.Login(this);
	}

	void Logout() {
		if (_ID < 0) return;
		TimerManager.Instance.Logout(_ID);
		_ID = -1;
	}

	void resetIdle() {
		_fIdleTime = 0;
	}
		
	/// <summary>
	/// DO NOT USE. This is for Timer to update.
	/// </summary>
	//[System.Obsolete("This is for Timer to update, not for manally use", true)]
	public void _CountDownTime(float deltaTime) {
		if (!_bCountingTime) return;
		_fCurrentTime += deltaTime;
		if (_bRepeatable) return;
		if (_fCurrentTime >= _fTime) {
			_bCountingTime = false;
		}
	}

	/// <summary>
	/// DO NOT USE. This is for Timer to update.
	/// </summary>
	//[System.Obsolete("This is for Timer to update, not for manally use", true)]
	public void _IdleTime(float deltaTime) {
		_fIdleTime += deltaTime;
		if (_bFixed) return;
		if (_fIdleTime >= _fTime * 3 + 1) {
			StopTimer();
		}
	}
}
