﻿using UnityEngine;
using System.Collections;
using TDGame.ObjectPooling;
using TDGame.Turrets;
using TDGame.Enemys;

public class TempMissleLauncher : MonoBehaviour {
	[SerializeField] GameObject m_Missle = null;
	[SerializeField] bool m_Fire = false;
	[SerializeField] Transform[] LaunchPoint = null;
	[SerializeField] GameObject Target = null;
	[SerializeField] float m_Time = 0.2f;
	[SerializeField] Vector3 m_Up = Vector3.up;
	[SerializeField] float m_OutForce = 7;

	ObjectPool m_MisslePool;
	bool launching = false;

	void Awake() {
		m_MisslePool = ObjectPoolManager.CreatePool(m_Missle);
	}

	void Update() {
		if (Target.activeInHierarchy)
			transform.LookAt(Target.transform.position, m_Up);

		if (!m_Fire) return;
		//m_Fire = false;

		if (launching) return;
		launching = true;
		StartCoroutine(QueueLaunch(m_Time));
	}

	void Launch(int index) {
		Missle missle = m_MisslePool.create().GetComponent<Missle>();
		missle.transform.forward = Vector3.up;
		missle.transform.position = LaunchPoint[index].position;
		missle.GetComponent<Rigidbody>().AddForce(Vector3.up * m_OutForce, ForceMode.Impulse);
		if (Target.GetComponent<Enemy>() == null || Target.GetComponent<Enemy>().isDead()) PickTarget();
		missle.m_Target = Target;
	}

	void PickTarget() {
		Collider[] colliders = Physics.OverlapSphere(transform.position, 25);
		for (int i = 0; i < colliders.Length; i ++) {
			Enemy enemy = colliders[i].GetComponent<Enemy>();
			if (enemy == null) continue;
			Target = enemy.gameObject;
			break;
		}
	}

	IEnumerator QueueLaunch(float time) {
		for (int i = 0; i < LaunchPoint.Length; i ++) {
			Launch(i);
			yield return new WaitForSeconds(time);
		}

		launching = false;
	}
}
