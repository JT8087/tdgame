﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TDGame.Enemys;
using TDGame.ObjectPooling;

namespace TDGame.Turrets
{
	public class MissleTurretV2 : Turret
	{
		[Header("Missle Turret Settings")]
		[SerializeField] float m_MinRange;
		[SerializeField] float m_MissleThrustDelay = 1;
		[SerializeField] float m_FireAngle = 5;
		[SerializeField] float m_FireRate = 1;
		[SerializeField] float m_LaunchForce = 7;
		[SerializeField] float m_LockingTime = 2;
		[SerializeField] float m_ReloadTime = 4;
		[SerializeField] GameObject m_Missle = null;
		[SerializeField] Transform m_LaserPoint;
		[SerializeField] Transform[] m_LaunchPoints;

		static ObjectPool _MisslesPool;
		LineRenderer m_Laser;
		Timer m_LockingTimer;
		Timer m_ReloadTimer;
		Timer m_FireTimer;
		bool m_TargetLocked;
		int _FireQueue;

		// -------------------------------------------------------------------------
		// Unity event funcitons
		void Start() {
			FireQueue = 0;
			m_LockingTimer = new Timer(m_LockingTime);
			m_ReloadTimer = new Timer(m_ReloadTime);
			m_FireTimer = new Timer(1 / m_FireRate, true);
			m_Laser = GetComponent<LineRenderer>();
		}

		void Update() {
			if (m_Overriding) return;
			// Pick up a target
			// If can't pick a target, stop here.
			if (!isValidTarget(m_Target) && !PickTarget()) {
				return;
			}
			// Get the target position.
			Vector3 targetPosition = m_Target.transform.position + m_Target.m_HitOffset;
			// ...take aim...
			Aiming(targetPosition);
			// ... and shoot
			Shoot();
		}

		// ------------------------------------------------------------------

		/// <summary>
		/// Shoot a target.
		/// </summary>
		protected override void Shoot() {
			Vector3 dir = m_Target.transform.position - m_TurrentPivot.position;
			dir = m_TurrentPivot.InverseTransformDirection(dir);
			float angle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg;
			if (Mathf.Abs(angle) < m_FireAngle) {
				if (LockingTarget()) {
					startShooting();
				}
			} else {
				ceaseShooting();
			}
		}

		bool LockingTarget() {
			if (!m_LockingTimer.isActivated) {
				m_LockingTimer.StartTimer();
			};
			m_Laser.enabled = true;
			m_Laser.SetPosition(0, m_LaserPoint.position);
			m_Laser.SetPosition(1, m_Target.transform.position + m_Target.m_HitOffset);
			return m_LockingTimer.isTimingDone;
		}

		protected override bool isWithInRange(Vector3 target) {
			Vector3 distance = target - transform.position;
			float sqrDistance = distance.sqrMagnitude;
			float maxRange = m_Range * m_Range;
			float minRange = m_MinRange * m_MinRange;
			if (sqrDistance <= maxRange && sqrDistance >= minRange) 
				return true;

			return false;
		}

		public override void Override(Ray direction, bool attack) {

		}

		bool isValidTarget(Enemy target) {
			if (target == null) return false;
			return (!target.isDead() && !target.isIdle());
		}

		void startShooting() {
			Debug.DrawLine(transform.position, m_Target.transform.position);
			if (m_ReloadTimer.isTiming) return;
			int times = m_FireTimer.GetRepeatedTimes();

			for (int i = 0; i < times; i ++) {
				Missle missle = MisslesPool.create().GetComponent<Missle>();
				Transform missleTransform = missle.GetComponent<Transform>();
				Rigidbody missleRigidbody = missle.GetComponent<Rigidbody>();
				Collider missleCollider = missle.GetComponent<Collider>();

				missleTransform.position = m_LaunchPoints[FireQueue].position;
				missleTransform.rotation = m_LaunchPoints[FireQueue].rotation;
				Physics.IgnoreCollision(missleCollider, this.GetComponent<Collider>());
				missle.m_Target = m_Target.gameObject;
				missleRigidbody.AddForce(missleTransform.forward * m_LaunchForce, ForceMode.Impulse);
				FireQueue ++;
			}
		}

		void ceaseShooting() {
			m_Laser.enabled = false;
			m_LockingTimer.StopTimer();
			if (m_FireTimer.isActivated) {
				m_ReloadTimer.StartTimer();
			}
			m_FireTimer.StopTimer();
			FireQueue = 0;
		}

		int FireQueue {
			get { return _FireQueue; }
			set {
				_FireQueue = value;
				if (_FireQueue < 0) _FireQueue = m_LaunchPoints.Length - 1;

				if (_FireQueue >= m_LaunchPoints.Length) {
					m_FireTimer.StopTimer();
					m_ReloadTimer.StartTimer();
					_FireQueue = 0;
				}
			}
		}

		ObjectPool MisslesPool {
			get {
				if (_MisslesPool == null) {
					_MisslesPool = ObjectPoolManager.CreatePool(m_Missle);
				}

				return _MisslesPool;
			}
		}
	}
}