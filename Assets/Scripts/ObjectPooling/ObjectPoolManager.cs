﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TDGame.ObjectPooling
{
	public static class ObjectPoolManager {
		static List<ObjectPool> m_ObjectPools = new List<ObjectPool>();

		public static ObjectPool CreatePool(GameObject prefab) {
			int index = FindSamePrefab(prefab);
			if (index >= 0) return m_ObjectPools[index];

			ObjectPool pool = new ObjectPool(prefab);
			m_ObjectPools.Add(pool);
			return pool;
		}

		static int FindSamePrefab(GameObject prefab) {
			for (int i = 0; i < m_ObjectPools.Count; i ++) {
				if (m_ObjectPools[i].Prefab.Equals(prefab)) {
					return i;
				}
			}
			return -1;
		}
	}
}

