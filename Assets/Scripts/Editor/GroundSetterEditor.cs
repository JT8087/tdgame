﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(GroundSetter))]
[CanEditMultipleObjects]
public class GroundSetterEditor : Editor {
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		GroundSetter setter = target as GroundSetter;

		if (GUILayout.Button("Build")) {
			setter.Resize();
		}
	}
}
