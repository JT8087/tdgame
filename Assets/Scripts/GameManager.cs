﻿using UnityEngine;
using System.Collections;
using TDGame.ObjectPooling;
using TDGame.Enemys;
using TDGame.Turrets;

public class GameManager : MonoBehaviour {
	static int m_Money = 100;

	[SerializeField] GameObject m_Walker = null; 
	public string[] m_HitLayer;

	ObjectPool m_WalkersPool;
	CameraTransitionHandler m_CameraHandler;
	OverridingControl m_Overrider;
	Turret m_OverridedTurret;
	bool m_IsOverriding;

	void Awake()
	{
		m_WalkersPool = ObjectPoolManager.CreatePool(m_Walker);
		m_CameraHandler = GetComponent<CameraTransitionHandler>();
		m_Overrider = Camera.main.transform.GetComponentInParent<OverridingControl>();
		InvokeRepeating("spawnWalkers", 0, 0.6f);
	}

	void Update()
	{
		if (m_IsOverriding && m_OverridedTurret != null) {
			bool attack = m_Overrider.firing();
			Ray ray = m_Overrider.aiming();
			m_OverridedTurret.Override(ray, attack);
		}

		if (Input.GetButtonDown("Fire1")) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit, Mathf.Infinity, LayerMask.GetMask(m_HitLayer))) {
				Turret turret = hit.transform.GetComponent<Turret>();
				if (turret != null) {
					StartCoroutine(TranslatingToTurret(turret));
				}
			}
		}

		if (Input.GetButtonDown("Fire2")) {
			// If is in transition, just quit.
			if (m_CameraHandler.isTranslating) return;

			m_CameraHandler.MoveToMapView();
			m_Overrider.m_Overriding = false;
			m_Overrider.LockCursor = false;
			m_IsOverriding = false;

			if (m_OverridedTurret != null) {
				m_OverridedTurret.m_Overriding = false;
				m_OverridedTurret = null;
			}
		}
	}

	public static void enemyReached(int damage)
	{
		m_Money -= damage;
		//print("You have been hit! Damage:" + damage);
	}

	public static void enemyEliminated(int reward)
	{
		m_Money += reward;
		//print("You have been rewarded! Money:" + m_Money);
	}

	void spawnWalkers()
	{
		GameObject walker = m_WalkersPool.create();
		// If can't get a object from pool, just return.
		if (walker == null) return;
		walker.transform.position = WayPoints.WaypointSet[0].position;
		walker.transform.rotation = WayPoints.WaypointSet[0].rotation;
	}

	IEnumerator TranslatingToTurret(Turret turret)
	{
		// If already in overriding,
		if (m_IsOverriding) yield break;

		// Start overriding
		m_IsOverriding = true;

		// Start moving
		m_CameraHandler.MoveToTurretView(turret.transform);

		// If it doesn't start moving, quit this coroutine.
		if (!m_CameraHandler.isTranslating) yield break;

		// Wait till the transition finished.
		while (m_CameraHandler.isTranslating) yield return null;

		// Setting and enable overrider.
		m_Overrider.m_Overriding = true;
		m_Overrider.LockCursor = true;
		m_Overrider.Reseting();
		m_OverridedTurret = turret;
		m_OverridedTurret.m_Overriding = true;
	}
}
