﻿using UnityEngine;
using System.Collections;

public class PlayerControlCamera : MonoBehaviour {
	[SerializeField] bool m_LockCursorInStart = true;
	[SerializeField][MinMaxSlider(-90, 90)] Vector2 m_TiltAngleLimit = new Vector2(-70, 70);
	[SerializeField][Range(1, 15)] float m_MouseSensitiveness = 1;
	[SerializeField][Range(0, 10)] float m_CameraTransferSpeed = 1;
	[SerializeField] Camera m_MainCamera = null;
	[SerializeField] SwappableCharacter m_ControlledCharacter = null;

	Vector3 m_CameraEuler;
	Transform m_CameraTransform;
	Transform m_CameraPivot;

	float m_LookAngle;
	float m_TiltAngle;
	bool m_Transferring;

	//-------------------------------------------------------------------------
	// Unity functions

	void Awake() {
		// Find main camera and do some initialization
		m_MainCamera = Camera.main;
		m_CameraTransform = m_MainCamera.transform;
		m_CameraEuler = m_CameraTransform.eulerAngles;
		m_CameraTransform.localRotation = Quaternion.identity;

		// If can't find pivot, log a warning and disable this behaviour.
		m_CameraPivot = m_CameraTransform.parent;
		if (m_CameraPivot == null) {
			Debug.LogWarning("MainCamera needs a pivot GameObject to be it's parent in order to make this MonoBehaviour work!\nThis MonoBehaviour will be turn off immediately.");
			this.enabled = false;
		}

		// If didn't specify a character, try to find one from scene.
		if (m_ControlledCharacter == null)
		m_ControlledCharacter = FindObjectOfType<SwappableCharacter>();

		// If still can't find one, log a warning and disable this behaviour.
		if (m_ControlledCharacter == null) {
			Debug.LogWarning("Can't find any activated SwitchableCharacter in current scene!\nThis MonoBehaviour will be turn off immediately.");
			this.enabled = false;
		}

		// Setting cursor's appearance
		LockCursor = m_LockCursorInStart;

		// Move camera to the character and control it.
		m_CameraPivot.position = m_ControlledCharacter.transform.position;
		m_CameraPivot.rotation = m_ControlledCharacter.transform.rotation;
		m_ControlledCharacter.StartOverride();
	}

	void Update() {
		// If player is transferring to a character, do not and just return.
		if (m_Transferring) return;

		// Get raw input from mouse and keyboard.
		Vector2 mouseInput = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
		Vector2 keyboardInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

		// Move camera and get facing direction.
		Vector3 direction = HandleViewing(mouseInput);

		// Convert input to movement direction.
		Vector3 movement = ConvertKeyboardInput(keyboardInput);

		// Sending directions to character.
		m_ControlledCharacter.Overriding(direction, movement);

		// Move camera to character's position
		FollowCharacter();

		// Check if there's need to transfer to other character.
		HandleTransfer();
	}

	//--------------------------------------------------------------------------
	// Handlers

	// Move camera by input and return current facing direciton.
	Vector3 HandleViewing(Vector2 movement) {
		movement *= m_MouseSensitiveness;

		m_LookAngle += movement.x;
		Quaternion pivotTargetRotation = Quaternion.Euler(0, m_LookAngle, 0);

		m_TiltAngle -= movement.y;
		m_TiltAngle = Mathf.Clamp(m_TiltAngle, m_TiltAngleLimit.x, m_TiltAngleLimit.y);
		Quaternion cameraTargetRotation = Quaternion.Euler(m_TiltAngle, m_CameraEuler.y, m_CameraEuler.z);

		m_CameraPivot.localRotation = pivotTargetRotation;
		m_CameraTransform.localRotation = cameraTargetRotation;

		Debug.DrawRay(m_CameraTransform.position, m_CameraTransform.forward * 100, Color.black);
		return m_CameraTransform.forward;
	}

	// Convert raw input to movement direction.
	Vector3 ConvertKeyboardInput(Vector2 movement) {
		Vector3 worldMovement = new Vector3(movement.x, 0, movement.y);
		if (worldMovement.sqrMagnitude > 1) worldMovement.Normalize();
		worldMovement = m_ControlledCharacter.transform.TransformDirection(worldMovement);
		return worldMovement;
	}
		
	// Detect transfer and handle it.
	void HandleTransfer() {
		// If press C, swtich cursor lock state.
		if (Input.GetKeyDown(KeyCode.C)) LockCursor = !LockCursor;

		// If didn't press E, can just return from here.
		if (!Input.GetKeyDown(KeyCode.E)) return;

		// If cursor is locked, find entitys along the middle point of camera.
		// Otherwise find entitys along cursor
		Ray ray;
		if (LockCursor)
			ray = m_MainCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
		else
			ray = m_MainCamera.ScreenPointToRay(Input.mousePosition);

		// If find an entity and it have SwappableCharacter attached, transfer to it.
		RaycastHit hitInfo;
		if (Physics.Raycast(ray, out hitInfo)) {
			SwappableCharacter character = hitInfo.transform.GetComponent<SwappableCharacter>();
			if (character == null || character == m_ControlledCharacter) return;

			StartCoroutine(Transferring(character));
		}
	}

	//--------------------------------------------------------------------------
	// Misc.

	// Move camera to character.
	void FollowCharacter() {
		m_CameraPivot.position = m_ControlledCharacter.transform.position;
	}

	// The Coroutine handle transferring.
	IEnumerator Transferring(SwappableCharacter character) {
		m_Transferring = true;					// Set transferring flag to true.
		character.StartOverride();				// Tell the transfer-to character it's about to get control
		m_ControlledCharacter.QuitOverride();	// Tell the transfer-from character it's free to go.

		// Save current position and rotation of camera.
		Vector3 oldPosition = m_CameraPivot.position;
		Quaternion oldRotation = m_CameraPivot.rotation;

		// Save transfer-to character's position and rotation.
		Vector3 newPosition = character.transform.position;
		Quaternion newRotation = character.transform.rotation;

		// Start of transfer.
		float time = 0;
		while (time < 1) {
			time += Time.deltaTime * m_CameraTransferSpeed;
			if (time > 1) time = 1;
			m_CameraPivot.position = Vector3.Slerp(oldPosition, newPosition, time);
			m_CameraPivot.rotation = Quaternion.Slerp(oldRotation, newRotation, time);
			yield return null;
		}
		// End of transfer.

		// Setting varibles to the new character.
		m_LookAngle = m_CameraPivot.eulerAngles.y;
		m_ControlledCharacter = character;

		// Set transferring flag to false.
		m_Transferring = false;
	}

	public bool LockCursor {
		get { return Cursor.lockState == CursorLockMode.Locked; }
		set {
			Cursor.lockState = value? CursorLockMode.Locked: CursorLockMode.None;
			Cursor.visible = !value;
		}
	}
}
