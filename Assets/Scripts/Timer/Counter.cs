//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//
//namespace TDGame.Timer
//{
//	public class Counter : Timer
//	{
//		protected bool repeat;
//		protected float countTime;
//		
//		public Counter()
//		{
//			repeat = false;
//			countTime = 0;
//		}
//
//		// ----------------------------------------------------------------------
//		// Apis
//
//		public override void reset()
//		{
//			// If this timer isn't able to access, just return.
//			if (!isAbleToAccess()) return;
//			passedTime = 0;
//			countTime = 0;
//		}
//
//		public virtual float CountDownTime
//		{
//			get {
//				isAbleToAccess();
//				return countTime;
//			}
//			set {
//				// If this timer isn't able to access, just return.
//				if (!isAbleToAccess()) return;
//				countTime = value;
//			}
//		}
//
//		/// <summary>
//		/// Return the Counter how many times it has counted down to.
//		/// </summary>
//		/// <param name="clearPassedTime">Clear passed time.</param>
//		public virtual int getCount(bool clearPassedTime = false)
//		{
//			// If this timer isn't able to access, return -1.
//			if (!isAbleToAccess()) return -1;
//
//			// How many times it has counted down to.
//			int times = (int)(passedTime / countTime);
//
//			// Minus those already passed time.
//			if (clearPassedTime)
//				passedTime -= countTime * times;
//
//			// If this counter isn't set to repeat, pause it.
//			if (times > 0 && !repeat) {
//				pause = true;
//			}
//
//			return times;
//		}
//
//		/// <summary>
//		/// Return how much time has counted down by percent
//		/// </summary>
//		/// <param name="clearPassedTime">Clear passed time.</param>
//		public virtual float getPercent(bool clearPassedTime = false)
//		{
//			// If this timer isn't able to access, just return.
//			if (!isAbleToAccess()) return -1;
//
//			// How much it has counted.
//			float percent = passedTime / countTime;
//
//			// Minus those already passed time.
//			if (clearPassedTime) {
//				passedTime -= countTime * (int)percent;
//			}
//
//			// If this counter isn't set to repeat, pause it.
//			if (percent >= 1 && !repeat) {
//				pause = true;
//			}
//
//			return percent;
//		}
//
//		public virtual bool Repeat
//		{
//			get {
//				isAbleToAccess();
//				return repeat;
//			}
//
//			set {
//				if (!isAbleToAccess()) return;
//				repeat = value;
//			}
//		}
//	}
//}