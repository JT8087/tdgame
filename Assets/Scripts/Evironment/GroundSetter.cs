﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[ExecuteInEditMode]
public class GroundSetter : MonoBehaviour
{
	[Header("Setting once")]
	[SerializeField] Transform   m_Ground = null;
	[SerializeField] Transform	 m_BaseFolder = null;
	[SerializeField] GameObject  m_BasePrefab = null;

	[Header("Configurable")]
	[Range(1, 60)][SerializeField]  int m_SizeX = 10;
	[Range(1, 60)][SerializeField]  int m_SizeY = 10;

	[SerializeField] GameObject[] m_Bases;

	private float m_BaseSize;
	private float m_BaseThickness;

	public void Resize()
	{
		m_BaseSize = m_BasePrefab.transform.localScale.x;
		m_BaseThickness = m_BasePrefab.transform.localScale.y;
		m_Ground.localScale = new Vector3(m_SizeX * m_BaseSize, 1, m_SizeY * m_BaseSize);
		GlobalVariable.Instance.MapWidth = m_SizeX;
		GlobalVariable.Instance.MapLength = m_SizeY;

		ClearBases();
		CreateBases();
	}

	void ClearBases()
	{
		if (m_Bases == null) return;
		foreach (var item in m_Bases) {
			if (item != null)
				DestroyImmediate(item.gameObject);
		}
	}
		
	void CreateBases()
	{
		if (m_BaseFolder == null) {
			m_BaseFolder = new GameObject("Bases").transform;
			m_BaseFolder.parent = transform;
		}

		m_Bases = new GameObject[m_SizeX * m_SizeY];

		float x = BaseStartX();
		float y = m_Ground.position.y + m_BaseThickness;
		float z = BaseStartY();

		for (int i = 0; i < m_SizeY; i++) {
			for (int j = 0; j < m_SizeX; j++) {
				GameObject temp = Instantiate(m_BasePrefab);

				// Link this instance to prefab
				temp = PrefabUtility.ConnectGameObjectToPrefab(temp.gameObject, m_BasePrefab);

				temp.transform.position = new Vector3(x, y, z);
				temp.transform.parent = m_BaseFolder;
				m_Bases[j + i * m_SizeX] = temp;
				x -= m_BaseSize;
			}

			x = BaseStartX();
			z -= m_BaseSize;
		}

	}

	float BaseStartX()
	{
		return (m_SizeX * m_BaseSize - m_BaseSize)/2 + m_Ground.position.x;
	}

	float BaseStartY()
	{
		return (m_SizeY * m_BaseSize - m_BaseSize)/2 + m_Ground.position.x;
	}
}

#endif