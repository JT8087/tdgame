﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class TimeCounter : MonoBehaviour {
	static TimeCounter _instance;
	static List<Counter> m_CounterList;

	void Awake() {
		m_CounterList = new List<Counter>();
	}

	void Update() {
		for (int i = 0; i < m_CounterList.Count; i ++) {
			if (m_CounterList[i].isCounting) {
				m_CounterList[i].Count(Time.deltaTime);
			}
		}
	}

	public int CreateCounter(float time) {
		for (int i = 0; i < m_CounterList.Count; i ++) {
			if (!m_CounterList[i].isCounting) {
				m_CounterList[i].StartCounting(time);
				return i;
			}
		}

		m_CounterList.Add(new Counter(time));
		return m_CounterList.Count - 1;
	}

	public bool Counting(int counterIndex) {
		return m_CounterList[counterIndex].isCountingDone;
	}

	public static TimeCounter Instance {
		get {
			if (_instance == null) {
				_instance = FindObjectOfType<TimeCounter>();
			}
			if (_instance == null) {
				GameObject timeCounter = new GameObject("TimeCounter");
				_instance = timeCounter.AddComponent<TimeCounter>();
			}

			return _instance;
		}
	}

	// --------------------------------------------------------------
	// Counter struct

	struct Counter {
		float CountingTime;
		bool Counting;

		public Counter(float time) {
			Counting = true;
			CountingTime = time;
		}

		public void StartCounting(float time) {
			Counting = true;
			CountingTime = time;
		}

		public bool isCounting {
			get {
				return Counting;
			}
		}

		public bool isCountingDone {
			get {
				if (Counting && CountingTime <= 0) {
					Counting = false;
					return true;
				}
				return false;
			}
		}

		public void Count(float deltaTime) {
			CountingTime -= deltaTime;
		}
	}
}
