﻿using UnityEngine;
using System.Collections;
using TDGame.Enemys;
using TDGame.ObjectPooling;

namespace TDGame.Turrets
{
	public class Missle : MonoBehaviour, IPoolableObject {
		public GameObject m_Target = null;

		[SerializeField] float m_FlySpeed = 10;
		[SerializeField] float m_MaxSpeed = 100;
		[SerializeField] float m_ExplodeRange = 5;
		[SerializeField] float m_ExplodeDamage = 120;
		[SerializeField] bool m_AutoTrack = false;
		[SerializeField] bool m_PerfectTracking = false;
		[SerializeField] bool m_AutoIgnite = true;
		[SerializeField] float m_ThrustingDelay = 1f;
		[SerializeField] float m_TrackingSpeed = 0.08f;
		[SerializeField] float m_FlightTrackingSpeed = 0.001f;
		[SerializeField] Transform m_Warhead = null;
		[SerializeField] Collider m_Trigger = null;
		[SerializeField] ParticleSystem m_ExplodeEffect = null;
		[SerializeField] ParticleSystem[] m_FlightEffect = null;

		[Header("Settings for object pooling")]
		[SerializeField] Renderer[] m_Renderers = null;
		[SerializeField] float m_RecycleDelay = 1;
		[SerializeField] float m_AutoRecycleTime = 10;

		bool delayRecycling = false;
		bool thrusting = false;
		Rigidbody m_Rigidbody;

		Vector3 m_LastPosition;
		Vector3 m_LastVelocity;
		Vector3 m_PredictPosition;

		ObjectPool pool;
		int id;

		void Awake() {
			m_Rigidbody = GetComponent<Rigidbody>();
			m_ExplodeEffect.transform.localScale *= m_ExplodeRange;
			reset();
		}

		void FixedUpdate() {
			if (!delayRecycling) {
				Flight();
				TrackTarget();
			}
		}

		void Update() {
			if (!delayRecycling && m_AutoTrack && m_Target != null) {
				m_PredictPosition = PredictTarget();
			}
		}

		void OnCollisionEnter() {
			Explode();
			delayRecycle();
		}

		void TrackTarget() {
			if (!m_AutoTrack || m_Target == null) return;

			Vector3 tarDirection;
			Vector3 direction = m_PredictPosition - transform.position;
			float trackingSpeed = 0;

			if (thrusting) {
				tarDirection = Vector3.Reflect(-m_Rigidbody.velocity, direction.normalized);
				trackingSpeed = m_FlightTrackingSpeed;
			} else {
				tarDirection = direction;
				trackingSpeed = m_TrackingSpeed;
			}

			if (m_PerfectTracking) {
				tarDirection = m_LastPosition - transform.position;
			}

//			Debug.DrawRay(transform.position, tarDirection);
//			Debug.DrawRay(transform.position, direction, Color.red);
//			Debug.DrawRay(transform.position, m_Rigidbody.velocity, Color.blue);

			m_Rigidbody.MoveRotation(Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, tarDirection, trackingSpeed, 0)));
		}

		void Flight() {
			if (delayRecycling || !thrusting) return;

			m_Rigidbody.AddRelativeForce(new Vector3(0, 0, m_FlySpeed));
			if (m_Rigidbody.velocity.sqrMagnitude > (m_MaxSpeed * m_MaxSpeed)) {
				m_Rigidbody.velocity = m_Rigidbody.velocity.normalized * m_MaxSpeed;
			}

			if (m_PerfectTracking) {
				m_Rigidbody.velocity = transform.forward * m_Rigidbody.velocity.magnitude;
			}
		}

		void Explode() {
			Collider[] colliders = Physics.OverlapSphere(m_Warhead.position, m_ExplodeRange);
			for (int i = 0; i < colliders.Length; i ++) {
				Enemy enemy = colliders[i].GetComponent<Enemy>();
				if (enemy == null) continue;
				float distance = Vector3.Distance(m_Warhead.position, enemy.transform.position);
				enemy.gotHit(Mathf.Lerp(0, m_ExplodeDamage, distance / m_ExplodeRange));
			}

			m_ExplodeEffect.transform.up = Vector3.up;
			m_ExplodeEffect.Play();
		}

		Vector3 PredictTarget() {
			// If target is disabled, just return last position.
			// So the m_LastPosition won't be update, then
			// missle will shot at the last position of target.
			if (!m_Target.activeInHierarchy) return m_LastPosition;

			// If missle haven't been speed up, just return target's current position.
			float currentSpeed = m_Rigidbody.velocity.sqrMagnitude;
			Vector3 targetPos = m_Target.transform.position;
			if (currentSpeed <= 1f) return targetPos;

			// If enabled prefect tracking, don't do these calculate,
			// just update last position and done.
			Vector3 predictPos = targetPos;
			if (!m_PerfectTracking) {
				m_LastVelocity += (targetPos - m_LastPosition);
				m_LastVelocity *= 0.5f;
				float angle = Vector3.Angle(m_LastVelocity, m_Rigidbody.velocity);
				if (angle > 90) angle = 180 - angle;
				angle = 1 + angle / angle;
				predictPos += (m_LastVelocity / Time.deltaTime);
			}

			m_LastPosition = targetPos;
			return predictPos;
		}

		void StartThrusting() {
			m_Rigidbody.useGravity = false;
			thrusting = true;

			foreach (ParticleSystem item in m_FlightEffect) {
				item.Play();
			}
		}

		void delayRecycle() {
			CancelInvoke();
			delayRecycling = true;
			m_Rigidbody.velocity = Vector3.zero;
			m_Rigidbody.Sleep();

			m_Trigger.enabled = false;
			foreach (Renderer item in m_Renderers) {
				item.enabled = false;
			}
			foreach (ParticleSystem item in m_FlightEffect) {
				item.Stop();
			}
			Invoke("recycle", m_RecycleDelay);
		}
			
		public bool isIdle() {
			return !gameObject.activeInHierarchy;
		}

		public void reset() {
			CancelInvoke();
			m_Trigger.enabled = true;
			foreach (Renderer item in m_Renderers) {
				item.enabled = true;
			}
			foreach (ParticleSystem item in m_FlightEffect) {
				item.Stop();
			}
			gameObject.SetActive(true);

			if (m_AutoIgnite) {
				StartCoroutine(ThrustReady());
			} else {
				Invoke("StartThrusting", m_ThrustingDelay);
			}
			thrusting = false;
			m_Rigidbody.useGravity = true;
			delayRecycling = false;
			Invoke("recycle", m_AutoRecycleTime);
		}

		public void recycle() {
			pool.unregister(id);
			gameObject.SetActive(false);
			m_Rigidbody.Sleep();
			delayRecycling = false;
		}

		IEnumerator ThrustReady() {
			while (m_Rigidbody.velocity.y >= -0.1f) 
				yield return null;
			StartThrusting();
		}

		public void register(ObjectPool pool, int id) {
			this.pool = pool;
			this.id = id;
		}
	}
}