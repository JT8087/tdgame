﻿using UnityEngine;
using System.Collections;

public class QueueTest : QueueBehaviour
{
//	// Use this for initialization
//	override protected void QueueAwake()
//	{
//		print("Awake" + Time.unscaledTime + "," + QueuePriority);
//	}
//
//    // Use this for initialization
//	override protected void QueueStart()
//    {
//		print("Start" + Time.unscaledTime + "," + QueuePriority);
//    }
    
    // Update is called once per frame
	override protected void QueueUpdate()
    {
		print("Update" + Time.unscaledTime + "," + QueuePriority);
    }

	// Update is called once per frame
	override protected void QueueFixedUpdate()
	{
		print("FixedUpdate" + Time.unscaledTime + "," + QueuePriority);
	}

	// Update is called once per frame
	override protected void QueueLateUpdate()
	{
		print("LateUpdate" + Time.unscaledTime + "," + QueuePriority);
	}
}

