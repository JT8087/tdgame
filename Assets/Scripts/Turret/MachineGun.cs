﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TDGame.Enemys;

namespace TDGame.Turrets
{
	public class MachineGun : Turret
	{
		[Header("Machine Gun Turret Settings")]
		[SerializeField] EffectGroup m_HittingEffect = null;
		[SerializeField] EffectGroup m_LeftBarrelFireEffect = null;
		[SerializeField] EffectGroup m_RightBarrelFireEffect = null;
		[SerializeField] float m_FireRate = 4f;

		const string k_FireName = "Fire";
		const string k_FireRateName = "FireRate";

		int m_FireParameter;
		Animator m_Animator;
		RaycastHit m_Hit;
		bool m_FiringBarrel;

		// -------------------------------------------------------------------------
		// Unity event funcitons
		void Start()
		{
			// Get Animator and setting it
			m_Animator = GetComponent<Animator>();
			m_Animator.SetFloat(k_FireRateName, m_FireRate);
			m_FireParameter = Animator.StringToHash(k_FireName);

			FireAnimation = false;
		}

		void Update()
		{
			if (m_Overriding) return;
			// Pick up a target
			// If can't pick a target, stop here.
			if (!PickTarget()) {
				FireAnimation = false;
				return;
			}
			// Get the target position.
			Vector3 targetPosition = m_Target.transform.position + m_Target.m_HitOffset;
			// ...take aim...
			Aiming(targetPosition);
			// ... and shoot
			Shoot();
		}

		// ------------------------------------------------------------------

		/// <summary>
		/// Shoot a target.
		/// </summary>
		protected override void Shoot()
		{
			// If have a target...
			if (m_Target != null

			// If hit something...
			&& Physics.Raycast(m_FireTransform.position, 
							   m_FireTransform.forward, 
							   out m_Hit, 
							   m_Range, 
							   LayerMask.GetMask(m_AttackHitLayer))

				// If the thing we hit is an enemy, start shooting
			&& m_Hit.transform.CompareTag("Enemy")) {
				FireAnimation = true;
			} else { // If not, cease shooting
				FireAnimation = false;
			}
		}

		public override void Override(Ray direction, bool attack)
		{
			// See what the camera currently pointing at.
			Physics.Raycast(
				direction, out m_Hit, Mathf.Infinity, 
				LayerMask.GetMask(m_AttackHitLayer));

			// If there's nothing, make up a target.
			if (m_Hit.transform == null) m_Hit.point = direction.GetPoint(10);
			else Debug.DrawLine(transform.position, m_Hit.point, Color.blue);

			// Turn turret to target
			Aiming(m_Hit.point);


			// If want to open fire
			if (attack) {
				// Get barrel direction
				direction.origin = m_FireTransform.position;
				direction.direction = m_FireTransform.forward;

				// See if it can hit something
				Physics.Raycast(
					direction, out m_Hit, Mathf.Infinity, 
					LayerMask.GetMask(m_AttackHitLayer));

				// If hit nothing, make up a hitting point.
				if (m_Hit.transform == null) m_Hit.point = direction.GetPoint(10);

				// Start shooting
				FireAnimation = true;
			} else {
				// If don't fire, cease possible previous shooting.
				FireAnimation = false;
			}
		}

		// Driven by animation event
		void startShooting(int leftBarrel)
		{
			// Enable fire effect
			if (leftBarrel == 1)
				m_LeftBarrelFireEffect.Play();
			else
				m_RightBarrelFireEffect.Play();
			m_FiringBarrel = !m_FiringBarrel;

			// if did't actually hit something, return here.
			if (m_Hit.transform == null) {
				m_HittingEffect.enabled = false;
				return;
			}

			// Setting hitting effect.
			m_HittingEffect.transform.position = m_Hit.point;
			m_HittingEffect.transform.forward = m_Hit.normal;
			m_HittingEffect.Play();

			// Damage target.
			Enemy enemy = m_Hit.transform.GetComponent<Enemy>();

			if (enemy != null) {
				enemy.gotHit(m_Damage);
				if (enemy.isDead()) m_Targets.Remove(enemy);
			}
		}

		// Driven by animation event
		void ceaseShooting(int leftBarrel)
		{
			// Disable effects
			FireAnimation = false;
			if (leftBarrel == 1)
				m_LeftBarrelFireEffect.StopLight();
			else
				m_RightBarrelFireEffect.StopLight();
		}

		bool FireAnimation
		{
			set {
				m_Animator.SetBool(m_FireParameter, value);
			}
		}
	}
}