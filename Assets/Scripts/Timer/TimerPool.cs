﻿//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//
//namespace TDGame.Timer
//{
//	public class TimerPool : MonoBehaviour
//	{
//		static List<Timer> m_TimerList = new List<Timer>(16);
//		static TimerPool m_Self;
//
//		// ----------------------------------------------------------------------
//		// Apis
//
//		/// <summary>
//		/// Create a new timer and get an id of it.
//		/// </summary>
//		/// <param name="time">Time.</param>
//		public static T create<T>() where T : Timer, new()
//		{
//			existenceWarning();
//			// Get one index of idle Counter
//			int tag = getAnIdleTimer<T>();
//
//			// Activate it.
//			m_TimerList[tag].activate = true;
//
//			return (T)m_TimerList[tag];
//		}
//			
//		/// <summary>
//		/// Recycle a timer.
//		/// </summary>
//		/// <param name="tag">Tag.</param>
//		public static void recycle<T>(T timer) where T : Timer
//		{
//			timer.activate = false;
//		}
//
//		/// <summary>
//		/// Loads the pool.
//		/// </summary>
//		/// <returns>The pool.</returns>
//		public static TimerPool loadPool()
//		{
//			// If pool already exist, just quit.
//			if (m_Self != null) return m_Self;
//
//			// Try to find a pool.
//			m_Self = Object.FindObjectOfType<TimerPool>();
//
//			// If can't find any, create one.
//			if (m_Self == null) {
//				GameObject timerPool = new GameObject();
//				timerPool.name = "TimerPool";
//				timerPool.transform.parent = null;
//				timerPool.transform.position = Vector3.zero;
//				timerPool.AddComponent<TimerPool>();
//
//				m_Self = timerPool.GetComponent<TimerPool>();
//			}
//
//			return m_Self;
//		}
//
//		// ----------------------------------------------------------------------
//		// Internal method
//
//		/// <summary>
//		/// Find an idle Counter and return it's index. 
//		/// </summary>
//		/// <returns>Index of an idle Counter.</returns>
//		protected static int getAnIdleTimer<T>() where T : Timer, new() {
//			for (int i = 0; i < m_TimerList.Count; i ++) {
//				if (m_TimerList[i].activate == false) {
//					if (m_TimerList[i].GetType() != typeof(T)) {
//						m_TimerList[i] = new T();
//					}
//					return i;
//				}
//			}
//
//			// If can't find an idle Counter, create a new one and return it.
//			m_TimerList.Add(new T());
//			return m_TimerList.Count - 1;
//		}
//
//		protected static void existenceWarning()
//		{
//			if (m_Self == null) {
//				Debug.LogWarning("Didn't have any instance of TimerPool!!! Timers will not work.");
//			}
//		}
//
//		// ----------------------------------------------------------------------
//		// Unity method
//
//		void Awake()
//		{
//			m_Self = this;
//			if (FindObjectsOfType<TimerPool>().Length > 1)
//			{
//				Debug.LogWarning("Have more than one TimerPool!!!");
//				Destroy(this);
//			}
//		}
//
//		void Update()
//		{
//			for (int i = 0; i < m_TimerList.Count; i ++) {
//				if (!m_TimerList[i].pause && m_TimerList[i].activate) {
//					m_TimerList[i].timing(Time.deltaTime);
//				}
//			}
//		}
//	}
//}
