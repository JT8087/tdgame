﻿using UnityEngine;
using System.Collections;

namespace TDGame.Launcher
{
	public abstract class Launcher : MonoBehaviour
	{
		[SerializeField]protected GameObject m_Bullet;
		[SerializeField]protected Transform m_Barrel;

		abstract protected void shoot();
	}
}
