﻿using UnityEngine;
using System.Collections;
/*
namespace TDGame.Launcher
{
	public class LauncherSpell : Launcher
	{
		//Current Dispersion, computed by charge effects and ammo.
		public float m_CurrentDisp;
		private float m_CurrentMinDisp;
		private float m_CurrentMaxDisp;
		private float m_CurrentDispDec;
		private float m_CurrentDispInc;

		//Stage value
		private float m_ChargeTime;
		private float m_ChargeValue;
		private float m_PowerUpTime;
		private float m_FireRounds;
		private float m_FireDelay;
		private bool  m_Charging;
		private bool  m_BurstFire;

		//Heat value
		public float m_Heat;
		public float m_HeatDownDelay;
		public float m_OverHeat;

		//Cooldown value
		public float m_Cooldown;

		[SerializeField] LaunchData m_LaunchData;

		private ChargeData m_ChargeData;
		//----------------------------------------------------------//
		//Unity method

		protected void Start()
		{
			initSpell();
			initLaunchSpell();
		}

		protected void FixedUpdate()
		{
			if (m_Activated)
			{
				spellProcess();
			}

			ComputeDisps();
			DecreaseDisp();
			DecreaseHeat();
			CountDownCooldown();
		}

		//--------------------------------------------------------------//
		//API

		public override void initializeSpellData(Transform fireTransform, Transform character, int spellID)
		{
			base.initializeSpellData(fireTransform, character, spellID);

			m_LaunchData = m_Manager.requestLaunchData(spellID);
			m_ChargeData = m_LaunchData.ChargeEffects;
			m_SpellName = m_LaunchData.Name;
		}

		/// <summary>
		/// Forceful stops the spell.
		/// </summary>
		public override void stopSpell()
		{
			switch (m_Stage)
			{
				case SpellStage.Ready:
				case SpellStage.Prepare:
					m_Stage = SpellStage.CoolDown;
					break;

				case SpellStage.Charge:
				case SpellStage.FullChar:
				case SpellStage.Cast:
				case SpellStage.CastDelay:
					ending();
					break;

				case SpellStage.CoolDown:
				case SpellStage.OverHeat:
					break;
			}

			m_Activated = false;
		}

		/// <summary>
		/// Turn on the spell. (Push the fire trigger)
		/// </summary>
		public override void spellOn()
		{
			if (checkState() == true)
			{
				m_Activated = true;
				//Running once here
				spellProcess();
			}
		}

		/// <summary>
		/// Turn off the spell. (Release the fire trigger)
		/// </summary>
		public override void spellOff()
		{
			switch (m_Stage)
			{
				case SpellStage.Ready:
				case SpellStage.Prepare:
					m_Stage = SpellStage.CoolDown;
					m_Activated = false;
					return;
					//break;

				case SpellStage.Charge:
				case SpellStage.FullChar:
					m_ChargeValue = chargeTimetoValue(m_ChargeTime);
					if (m_LaunchData.BurstFire)
					{
						//m_ChargeTime = 0;
						m_BurstFire = true;
					}
					else
					{
						ending();
						m_Activated = false;
					}
					break;

				case SpellStage.Cast:
				case SpellStage.CastDelay:
					if (!m_LaunchData.BurstFire)
					{
						ending();
						m_Activated = false;
					}
					break;

				case SpellStage.CoolDown:
				case SpellStage.OverHeat:
					m_Activated = false;
					break;
			}
		}

		/// <summary>
		/// Switch on the spell.
		/// </summary>
		public override void switchOn()
		{
			;
		}

		/// <summary>
		/// Switch off the spell.
		/// </summary>
		public override void switchOff()
		{
			stopSpell();
		}

		public override bool IsOverHeat
		{
			get
			{
				if (m_LaunchData.StandaloneHeat)
				{
					return m_OverHeat > 0;
				}
				else
				{
					return m_Manager.ManagerOverHeat > 0;
				}
			}
		}

		public override bool IsCooldown
		{
			get
			{
				if (m_LaunchData.StandaloneCooldown)
				{
					return m_Cooldown > 0;
				}
				else
				{
					return m_Manager.ManagerCoolDown > 0;
				}
			}
		}

		/// <summary>
		/// Gets the stage percent.
		/// </summary>
		public override float StagePercent
		{
			get
			{
				float stagePercent = 1;
				switch(m_Stage)
				{
					case SpellStage.Ready:
						break;

					case SpellStage.Prepare:
						stagePercent -= m_PowerUpTime / MultipledData(LauncherParameter.PowerUp);
						break;

					case SpellStage.Charge:
					case SpellStage.FullChar:
						//m_ChargeValue = chargeTimetoValue(m_ChargeTime);
						stagePercent = chargeTimetoValue(m_ChargeTime);
						break;

					case SpellStage.Cast:
					case SpellStage.CastDelay:
						stagePercent -= m_FireDelay / MultipledData(LauncherParameter.FireRate);
						break;

					case SpellStage.CoolDown:
						if (m_LaunchData.StandaloneCooldown)
							stagePercent -= m_Cooldown / MultipledData(LauncherParameter.CoolDown);
						else
							stagePercent -= m_Manager.ManagerCoolDown / MultipledData(LauncherParameter.CoolDown);
						break;

					case SpellStage.OverHeat:
						if (m_LaunchData.StandaloneHeat)
							stagePercent -= m_OverHeat / MultipledData(LauncherParameter.OverHeat);
						else
							stagePercent -= m_Manager.ManagerOverHeat / MultipledData(LauncherParameter.CoolDown);
						break;
				}

				return stagePercent;
			}
		}

		public override float Heat
		{
			get
			{
				//???throw new System.NotImplementedException();
				if (m_LaunchData.StandaloneHeat)
				{
					return m_Heat;
				}
				else
				{
					return m_Manager.ManagerHeat;
				}
			}
		}

		public override float Cooldown
		{
			get
			{
				//???throw new System.NotImplementedException();
				if (m_LaunchData.StandaloneCooldown)
				{
					return m_Cooldown;
				}
				else
				{
					return m_Manager.ManagerCoolDown;
				}
			}
		}

		//----------------------------------------------------------------------------//
		//Protected function

		/// <summary>
		/// The process of this spell.
		/// </summary>
		protected void spellProcess()
		{
			if (m_LaunchData.AutoFire && m_Stage == SpellStage.CoolDown)
			{
				if (!IsCooldown && 
					!IsOverHeat && 
					m_Manager.Mana >= m_LaunchData.ManaCost)
				{
					m_Stage = SpellStage.Ready;
				}
			}
			if (powerUping() == true)
			{
				if (charging() == true)
				{
					if (!m_LaunchData.BurstFire || m_BurstFire)
					{
						if (firing() == false)
						{
							ending();
						}
					}
				}
			}
		}

		/// <summary>
		/// Checks the state.
		/// </summary>
		/// <returns><c>true</c>, if spell is ready to fire, <c>false</c> otherwise.</returns>
		protected bool checkState()
		{
			if (!IsCooldown && 
				!IsOverHeat && 
				m_Manager.Mana >= m_LaunchData.ManaCost &&
				!m_Manager.SpellActivated)
			{
				if (m_Stage == SpellStage.CoolDown)
				{
					m_Stage = SpellStage.Ready;
				}

				return true;
			}

			return false;
		}

		/// <summary>
		/// Power-up-ing.
		/// </summary>
		/// <returns><c>true</c>, if reached the powerup time, <c>false</c> otherwise.</returns>
		protected bool powerUping()
		{
			//If just begin powerup
			if (m_Stage == SpellStage.Ready)
			{
				m_Stage = SpellStage.Prepare;
				m_PowerUpTime = m_LaunchData.PowerUp;
				//return false;
			}

			//Counting the time.
			m_PowerUpTime -= Time.deltaTime * m_Manager.MagicCastRate;
			if (m_PowerUpTime <= 0)
			{
				m_PowerUpTime = 0;
				return true;
			}

			return false;
		}

		/// <summary>
		/// Charging this instance.
		/// </summary>
		protected bool charging()
		{
			//If just begin charge.
			if (m_Stage == SpellStage.Prepare)
			{
				m_Stage = SpellStage.Charge;
				m_ChargeTime = m_LaunchData.Charge;
				//return false;
			}

			//Counting the charge.
			m_ChargeTime -= Time.deltaTime * m_Manager.MagicCastRate;
			if (m_ChargeTime <= 0)
			{
				m_ChargeTime = 0;
				return true;
			}

			if (m_BurstFire) return true;

			return false;
		}

		/// <summary>
		/// Firing this spell.
		/// </summary>
		protected bool firing()
		{
			//If just begin fire
			if (m_Stage == SpellStage.Charge)
			{
				m_FireRounds = (int)MultipledData(LauncherParameter.FireTimes);
				m_FireDelay = 0;
			}

			if (m_FireRounds > 0)
			{
				if (m_FireDelay <= 0)
				{
					if (!consumeMana()) return false;
					cast();
					IncreaseHeat();
					m_FireDelay = MultipledData(LauncherParameter.FireRate);
					IncreaseDisp();
					m_FireRounds --;
				}
				else
				{
					m_FireDelay -= Time.deltaTime * m_Manager.MagicCastRate;
				}
				return true;
			}

			return false;
		}

		/// <summary>
		/// Cast this spell.
		/// </summary>
		protected void cast()
		{
			m_Stage = SpellStage.Cast;

			//Calculate actual fire position and fire direction.
			Vector3 firePosition = m_FireTransform.position + m_LaunchData.FirePosOffset;

			//Calculate actual launch speed and launch rotation
			float launchSpeed = MultipledData(LauncherParameter.BulletSpeed);
			Vector3 launchRotate = m_LaunchData.BulletRotate * chargeAffect(ChargeEffect.BulletRotate);

			//Calculate actual object amount and dispersion
			int objectAmount = (int)(MultipledData(LauncherParameter.BulletAmount));

			//Calculate dispersion map by dispShape
			Vector3[] dispMap =  calculateDispersionMap(objectAmount, m_CurrentDisp);

			//Fire object(s)
			for (int i = 0; i < objectAmount; i ++)
			{
				Vector3 dispersedDirection = Vector3.zero;

				//Load the dispersion map that pre-calculated.
				//dispersedDirection = dispMap[i] + m_FireTransform.TransformDirection(m_LaunchData.FireDirOffset);
				//dispersedDirection = Vector3.Angle(dispMap[i], m_FireTransform.forward) + m_LaunchData.FireDirOffset;
				dispersedDirection = Quaternion.Euler(m_LaunchData.FireDirOffset) * dispMap[i];
				dispersedDirection.Normalize();

				//Spawns the projectile object
				GameObject magic = Instantiate(m_LaunchData.Projectile, firePosition, Quaternion.LookRotation(dispersedDirection)) as GameObject;
				Physics.IgnoreCollision(m_Manager.GetComponent<Collider>(), magic.GetComponent<Collider>());
				var p_Rigibody = magic.GetComponent<Rigidbody>();
				var p_Bullet = magic.GetComponent<Projectile>();

				if (p_Rigibody != null)
				{
					//launch the object
					p_Rigibody.velocity = dispersedDirection * launchSpeed;
					p_Rigibody.angularVelocity = p_Rigibody.transform.TransformDirection(launchRotate);
					if (p_Bullet != null)
					{
						p_Bullet.Range  = MultipledData(LauncherParameter.Range);
						p_Bullet.Damage = MultipledData(LauncherParameter.Damage);
						p_Bullet.Impulse = MultipledData(LauncherParameter.Impulse);
					}
					//magic.SendMessage("getChargeAffect", m_ChargeValue, SendMessageOptions.DontRequireReceiver);
					//magic.SendMessage("getBuffAddition", );
				}
			}
		}

		/// <summary>
		/// End this spell.
		/// </summary>
		protected void ending()
		{
			if (m_Stage != SpellStage.CoolDown)
			{
				m_BurstFire = false;

				SetCooldown();

				m_Stage = SpellStage.CoolDown;
				if (!m_LaunchData.AutoFire)
				{
					m_Activated = false;
				}
			}
		}

		//--------------------------------------------------//
		//Internal function

		/// <summary>
		/// Initializes the spell, set every data.
		/// </summary>
		void initLaunchSpell()
		{
			//Check projectile, if none then use default projetile.
			if (m_LaunchData.Projectile == null)
			{
				m_LaunchData.Projectile = Resources.Load<GameObject>("Projectiles/TestCube");
			}

			//If didn't have a LaunchData, create a default one.
			if (m_LaunchData == null)
			{
				m_LaunchData = new LaunchData();
			}

			//Reset every data.
			m_ChargeTime = 0f;
			m_ChargeValue = 0f;
			m_PowerUpTime = 0f;
			m_FireRounds = 0f;
			m_FireDelay = 0f;
			m_BurstFire = false;
			m_Heat = 0f;
			m_OverHeat = 0f;
			m_Cooldown = 0f;
		}

		/// <summary>
		/// Charges the affect.
		/// </summary>
		/// <returns>The affect.</returns>
		/// <param name="chargeEffect">Charge effect.</param>
		/// <param name="charge">Charge.</param>
		float chargeAffect(ChargeEffect chargeEffect)
		{
			//If can't charge or didn't charge, just return an one.
			if (m_LaunchData.Charge <= 0 || m_ChargeValue <= 0)
				return 1f;

			float effectValue = 1;

			switch (chargeEffect)
			{
				case ChargeEffect.BulletAmount:	effectValue = m_ChargeData.BulletAmount; break;
				case ChargeEffect.BulletRotate:	effectValue = m_ChargeData.BulletRotate; break;
				case ChargeEffect.BulletSpeed:	effectValue = m_ChargeData.BulletSpeed; break;
				case ChargeEffect.CoolDown:		effectValue = m_ChargeData.CoolDown; break;
				case ChargeEffect.Damage:		effectValue = m_ChargeData.Damage; break;
				case ChargeEffect.DispDec:		effectValue = m_ChargeData.DispDec; break;
				case ChargeEffect.DispInc:		effectValue = m_ChargeData.DispInc; break;
				case ChargeEffect.HeatDec:		effectValue = m_ChargeData.HeatDec; break;
				case ChargeEffect.HeatInc:		effectValue = m_ChargeData.HeatInc; break;
				case ChargeEffect.FireRate:		effectValue = m_ChargeData.FireRate; break;
				case ChargeEffect.FireTimes:	effectValue = m_ChargeData.FireTimes; break;
				case ChargeEffect.Impulse:		effectValue = m_ChargeData.Impulse; break;
				case ChargeEffect.ManaCost:		effectValue = m_ChargeData.ManaCost; break;
				case ChargeEffect.MaxDisp:		effectValue = m_ChargeData.MaxDisp; break;
				case ChargeEffect.MinDisp:		effectValue = m_ChargeData.MinDisp; break;
				case ChargeEffect.Range:		effectValue = m_ChargeData.Range; break;
			}

			return Mathf.Lerp(1f, effectValue, m_ChargeValue);
		}

		/// <summary>
		/// Charges the affect.
		/// </summary>
		/// <returns>The affect.</returns>
		/// <param name="value">Value.</param>
		/// <param name="charge">Charge.</param>
		float chargeAffect(float value)
		{
			//If can't charge or didn't charge, just return an one.
			if (m_LaunchData.Charge <= 0 || m_ChargeValue <= 0)
				return 1f;

			return Mathf.Lerp(1f, value, m_ChargeValue);
		}

		/// <summary>
		/// Charges the timeto value.
		/// </summary>
		/// <returns>The timeto value.</returns>
		/// <param name="chargeTime">Charge time.</param>
		float chargeTimetoValue(float chargeTime)
		{
			float chargeVolume = m_LaunchData.Charge;

			//If this spell can't charge, just return a 0.
			if (chargeVolume <= 0)
				return 0;

			return (chargeVolume - chargeTime) / chargeVolume;
		}

		//Calculate the dispersion map.(UNFINISHED)
		Vector3[] calculateDispersionMap(int objectAmount, float disp)
		{
			Vector3[] dispersionMap = new Vector3[objectAmount];

			switch (m_LaunchData.DispShape)
			{
				//For now I'd just use random spawn. will add other moduses late.
				case DispersionShape.Random:
					for (int i = 0; i < objectAmount; i ++)
					{
						dispersionMap[i] = ShotCal.GenerateRandomDisp(disp);
						dispersionMap[i] = m_FireTransform.TransformDirection(dispersionMap[i]);
					}
					break;
			}

			return dispersionMap;
		}

		bool consumeMana()
		{
			float manaCost = MultipledData(LauncherParameter.ManaCost);

			if (m_Manager.Mana < manaCost)
				return false;

			m_Manager.consumeMana(manaCost);
			return true;
		}

		float MultipledData(LauncherParameter parameter)
		{
			float effect = 1;
			switch (parameter)
			{
				case LauncherParameter.BulletAmount:
					effect = m_LaunchData.BulletAmount * chargeAffect(ChargeEffect.BulletAmount);
					break;

				case LauncherParameter.BulletSpeed:
					effect = m_LaunchData.BulletSpeed * chargeAffect(ChargeEffect.BulletSpeed);
					break;

				case LauncherParameter.Charge:
					effect = m_LaunchData.Charge;
					break;

				case LauncherParameter.CoolDown:
					effect = m_LaunchData.CoolDown * chargeAffect(ChargeEffect.CoolDown);
					break;

				case LauncherParameter.Damage:
					effect = m_LaunchData.Damage * chargeAffect(ChargeEffect.Damage) * m_Manager.MagicStrength;
					break;

				case LauncherParameter.DispDec:
					effect = m_LaunchData.DispDec * chargeAffect(ChargeEffect.DispDec) * m_Manager.MagicCastRate;
					break;

				case LauncherParameter.DispInc:
					effect = m_LaunchData.DispInc * chargeAffect(ChargeEffect.DispInc);
					break;

				case LauncherParameter.HeatDec:
					effect = m_LaunchData.HeatDec * chargeAffect(ChargeEffect.HeatDec) * m_Manager.MagicCastRate;
					break;

				case LauncherParameter.HeatInc:
					effect = m_LaunchData.HeatInc * chargeAffect(ChargeEffect.HeatInc);
					break;

				case LauncherParameter.HeatDownDelay:
					effect = m_LaunchData.HeatDownDelay;
					break;

				case LauncherParameter.OverHeat:
					effect = m_LaunchData.OverHeat;
					break;

				case LauncherParameter.FireRate:
					effect = m_LaunchData.FireRate * chargeAffect(ChargeEffect.FireRate) * m_Manager.MagicCastRate;
					break;

				case LauncherParameter.FireTimes:
					effect = m_LaunchData.FireTimes * chargeAffect(ChargeEffect.FireTimes);
					break;

				case LauncherParameter.Impulse:
					effect = m_LaunchData.Impulse * chargeAffect(ChargeEffect.Impulse) * m_Manager.MagicStrength;
					break;

				case LauncherParameter.ManaCost:
					effect = m_LaunchData.ManaCost * chargeAffect(ChargeEffect.ManaCost);
					break;

				case LauncherParameter.MaxDisp:
					effect = m_LaunchData.MaxDisp * chargeAffect(ChargeEffect.MaxDisp);
					break;

				case LauncherParameter.MinDisp:
					effect = m_LaunchData.MinDisp * chargeAffect(ChargeEffect.MinDisp);
					break;

				case LauncherParameter.PowerUp:
					effect = m_LaunchData.PowerUp;
					break;

				case LauncherParameter.Range:
					effect = m_LaunchData.Range * chargeAffect(ChargeEffect.Range);
					break;
			}

			return effect;
		}

		/// <summary>
		/// Compute the current disps.
		/// </summary>
		void ComputeDisps()
		{
			m_CurrentDispDec = MultipledData(LauncherParameter.DispDec);
			m_CurrentDispInc = MultipledData(LauncherParameter.DispInc);
			m_CurrentMaxDisp = MultipledData(LauncherParameter.MaxDisp);
			m_CurrentMinDisp = MultipledData(LauncherParameter.MinDisp);

			//If Max disp less than min disp, swap max and min.
			if (m_CurrentMaxDisp < m_CurrentMinDisp)
			{
				float temp = m_CurrentMinDisp;
				m_CurrentMinDisp = m_CurrentMaxDisp;
				m_CurrentMaxDisp = temp;
			}
		}

		/// <summary>
		/// Decreases the disp.
		/// </summary>
		void DecreaseDisp()
		{
			m_CurrentDisp -= Time.deltaTime * m_CurrentDispDec;

			if (m_CurrentDisp < m_CurrentMinDisp)
			{
				m_CurrentDisp = m_CurrentMinDisp;
			}
		}

		/// <summary>
		/// Increases the disp.
		/// </summary>
		void IncreaseDisp()
		{
			m_CurrentDisp += m_CurrentDispInc;
			if (m_CurrentDisp > m_CurrentMaxDisp)
			{
				m_CurrentDisp = m_CurrentMaxDisp;
			}
		}

		/// <summary>
		/// Decreases the heat.
		/// </summary>
		void DecreaseHeat()
		{
			//If this spell isn't heat standalone, leave this part to manager.
			if (!m_LaunchData.StandaloneHeat) return;

			if (m_HeatDownDelay > 0)
			{
				m_HeatDownDelay -= Time.deltaTime;
				return;
			}

			//Otherwise, calculate the heat.
			//If overheatted...
			if (m_OverHeat > 0)
			{
				m_OverHeat -= Time.deltaTime;
				return;
			}

			//Just decrease the heat
			if (m_Heat > 0)
			{
				m_Heat -= Time.deltaTime * MultipledData(LauncherParameter.HeatDec);
				if (m_Heat < 0) m_Heat = 0;
			}
		}

		/// <summary>
		/// Increases the heat.
		/// </summary>
		void IncreaseHeat()
		{
			//Just send the heat value to manager if this spell isn't heat standalone.
			if (!m_LaunchData.StandaloneHeat)
			{
				m_Manager.increaseHeat(MultipledData(LauncherParameter.HeatInc));
				return;
			}

			//Otherwise, calculate the heat.
			m_HeatDownDelay = MultipledData(LauncherParameter.HeatDownDelay);
			m_Heat += MultipledData(LauncherParameter.HeatInc);
			if (m_Heat >= 100)
			{
				m_Stage = SpellStage.OverHeat;
				stopSpell();
				m_Heat = 100;
				m_OverHeat = MultipledData(LauncherParameter.OverHeat);
			}
		}

		/// <summary>
		/// Sets the cooldown.
		/// </summary>
		void SetCooldown()
		{
			if (m_LaunchData.StandaloneCooldown)
			{
				m_Cooldown = MultipledData(LauncherParameter.CoolDown);
			}
			else
			{
				m_Manager.setCoolDown(MultipledData(LauncherParameter.CoolDown));
			}
		}

		/// <summary>
		/// Counts down the cooldown.
		/// </summary>
		void CountDownCooldown()
		{
			//If this spell isn't cooldown standalone, leave this part to manager.
			if (!m_LaunchData.StandaloneCooldown) return;

			//Otherwise, count down the cooldown.
			if (m_Cooldown > 0)
			{
				m_Cooldown -= Time.deltaTime * m_Manager.MagicCastRate;
				if (m_Cooldown < 0) m_Cooldown = 0;
			}
		}
	}
}
*/