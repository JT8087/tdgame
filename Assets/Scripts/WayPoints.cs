﻿using UnityEngine;
using System.Collections;

public sealed class WayPoints : MonoBehaviour {
	public static Transform[] WaypointSet;
	public Transform[] m_WayPoints;

	void Awake()
	{
		if (WaypointSet != null) {
			Debug.LogWarning("There are already one waypoints!!! ");
			Destroy(this);
		}

		WaypointSet = this.m_WayPoints;
	}
}
