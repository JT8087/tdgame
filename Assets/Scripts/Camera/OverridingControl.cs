﻿using UnityEngine;
using System.Collections;

public class OverridingControl : MonoBehaviour
{
	public bool m_Overriding = false;

	[Range(0f, 10f)] [SerializeField] float m_TurnSpeed = 1.5f;   // How fast the rig will rotate from user input.
	[SerializeField] float m_TurnSmoothing = 10f;                // How much smoothing to apply to the turn input, to reduce mouse-turn jerkiness
	[SerializeField] float m_TiltMax = 55f;                       // The maximum value of the x axis rotation of the pivot.
	[SerializeField] float m_TiltMin = 75f;                       // The minimum value of the x axis rotation of the pivot.

	[SerializeField] Transform m_CameraRig = null;
	[SerializeField] Transform m_CameraPivot = null;

	Vector3 m_PivotEulers;
	float m_LookAngle;
	float m_TiltAngle;

	void Awake()
	{
		m_PivotEulers = Vector3.zero;
	}

	void Update()
	{
		if (m_Overriding)
			turningRigAndPivot();
	}

	public void Reseting()
	{
		m_PivotEulers = m_CameraPivot.eulerAngles;
		m_LookAngle = 0;
		m_TiltAngle = 0;
	}

	void turningRigAndPivot()
	{
		// Get the input
		float x = Input.GetAxis("Mouse X");
		float y = Input.GetAxis("Mouse Y");

		m_LookAngle += x * m_TurnSpeed;
		Quaternion rigTargetRotation = Quaternion.Euler(0, m_LookAngle, 0);

		m_TiltAngle -= y * m_TurnSpeed;
		m_TiltAngle = Mathf.Clamp(m_TiltAngle, -m_TiltMin, m_TiltMax);
		Quaternion pivotTargetRotation = Quaternion.Euler(m_TiltAngle, m_PivotEulers.y, m_PivotEulers.z);

		if (m_TurnSmoothing > 0) {
			m_CameraPivot.localRotation = Quaternion.Slerp(m_CameraPivot.localRotation, pivotTargetRotation, m_TurnSmoothing * Time.deltaTime);
			m_CameraRig.localRotation = Quaternion.Slerp(m_CameraRig.localRotation, rigTargetRotation, m_TurnSmoothing * Time.deltaTime);
		} else {
			m_CameraPivot.localRotation = pivotTargetRotation;
			m_CameraRig.localRotation = rigTargetRotation;
		}

		Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.forward * 100);
	}

	public Ray aiming()
	{
		return new Ray(Camera.main.transform.position, Camera.main.transform.forward);
	}

	public bool firing()
	{
		return Input.GetButton("Fire1");;
	}

	public bool LockCursor
	{
		get {
			return Cursor.lockState == CursorLockMode.Locked;
		}

		set {
			Cursor.lockState = value ? CursorLockMode.Locked : CursorLockMode.None;
			Cursor.visible = !value;
		}
	}
}
