﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;

public class GlobalVariable : ScriptableObject
{
	static GlobalVariable _Instance;

	public bool IsMapView;
	public float MapWidth;
	public float MapLength;

	public static GlobalVariable Instance {
		get {
			if (_Instance == null) GetGlobalVariableSet();
			return _Instance;
		}
	}

	static void GetGlobalVariableSet()
	{
		#if UNITY_EDITOR
		object globalVariable = Resources.Load("GlobalVariables");
		if (globalVariable != null) {
			_Instance = Resources.Load<GlobalVariable>("GlobalVariables");
			return;
		}

		_Instance = ScriptableObject.CreateInstance<GlobalVariable>();
		string path = "Assets/Resources/GlobalVariables.asset";
		AssetDatabase.CreateAsset(_Instance, path);
		AssetDatabase.SaveAssets();
		#else
		_Instance = Resources.Load<GlobalVariable>("GlobalVariables");
		#endif
	}
}
