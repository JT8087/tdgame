﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TDGame.Enemys;
using TDGame.ObjectPooling;

namespace TDGame.Turrets
{
	public class MissleTurret : Turret
	{
		[Header("Missle Turret Settings")]
		[SerializeField] float m_MinRange = 0;
		[SerializeField] float m_MissleThrustDelay = 1;
		[SerializeField] float m_FireAngle = 5;
		[SerializeField] float m_FireRate = 1;
		[SerializeField] float m_LaunchForce = 7;
		[SerializeField] GameObject m_Missle = null;
		[SerializeField] Transform[] m_LaunchPoints;

		static ObjectPool _MisslesPool;
		bool m_ReadyToFire = true;
		int _FireQueue;

		// -------------------------------------------------------------------------
		// Unity event funcitons
		void Start() {
			FireQueue = 0;
		}

		void Update() {
			if (m_Overriding) return;
			// Pick up a target
			// If can't pick a target, stop here.
			if (!PickTarget()) {
				return;
			}
			// Get the target position.
			Vector3 targetPosition = m_Target.transform.position + m_Target.m_HitOffset;
			// ...take aim...
			Aiming(targetPosition);
			// ... and shoot
			Shoot();
		}

		// ------------------------------------------------------------------

		/// <summary>
		/// Shoot a target.
		/// </summary>
		protected override void Shoot() {
			Vector3 dir = m_Target.transform.position - m_TurrentPivot.position;
			dir = m_TurrentPivot.InverseTransformDirection(dir);
			float angle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg;
			if (Mathf.Abs(angle) < m_FireAngle) {
				startShooting();
			}
		}

		protected override bool isWithInRange(Vector3 target) {
			Vector3 distance = target - transform.position;
			float sqrDistance = distance.sqrMagnitude;
			float maxRange = m_Range * m_Range;
			float minRange = m_MinRange * m_MinRange;
			if (sqrDistance <= maxRange && sqrDistance >= minRange) 
				return true;
			
			return false;
		}

		public override void Override(Ray direction, bool attack) {
			
		}
			
		void startShooting() {
			Debug.DrawLine(transform.position, m_Target.transform.position);
			if (!m_ReadyToFire) return;

			Missle missle = MisslesPool.create().GetComponent<Missle>();
			Transform missleTransform = missle.GetComponent<Transform>();
			Rigidbody missleRigidbody = missle.GetComponent<Rigidbody>();
			Collider missleCollider = missle.GetComponent<Collider>();

			missleTransform.position = m_LaunchPoints[FireQueue].position;
			missleTransform.rotation = m_LaunchPoints[FireQueue].rotation;
			Physics.IgnoreCollision(missleCollider, this.GetComponent<Collider>());
			missle.m_Target = m_Target.gameObject;
			missleRigidbody.AddForce(missleTransform.forward * m_LaunchForce, ForceMode.Impulse);
			FireQueue ++;
			StartCoroutine(fireDelay());
		}
			
		void ceaseShooting() {
			
		}

		IEnumerator fireDelay() {
			m_ReadyToFire = false;
			float time = 0;
			while (time < 1) {
				time += Time.deltaTime * m_FireRate;
				yield return null;
			}

			m_ReadyToFire = true;
		}

		int FireQueue {
			get { return _FireQueue; }
			set {
				_FireQueue = value;
				if (_FireQueue >= m_LaunchPoints.Length) _FireQueue = 0;
				if (_FireQueue < 0) _FireQueue = m_LaunchPoints.Length - 1;
			}
		}

		ObjectPool MisslesPool {
			get {
				if (_MisslesPool == null) {
					_MisslesPool = ObjectPoolManager.CreatePool(m_Missle);
				}

				return _MisslesPool;
			}
		}
	}
}