﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text;

public class UnitTest : MonoBehaviour
{
	[SerializeField]Text debugText = null;

	// Update is called once per frame
	void Update ()
	{
		debugText.text = Input.mousePosition.ToString();
		BuildMenu.Current.MoveInPosition(Input.mousePosition);
	}
}
