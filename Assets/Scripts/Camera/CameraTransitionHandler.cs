﻿using UnityEngine;
using System.Collections;

public class CameraTransitionHandler : MonoBehaviour
{
	// -------------------------------------------------
	// This struct is for store both position and rotation of a transform.
	[System.Serializable]
	public struct Transformation
	{
		[SerializeField]Vector3 position;
		[SerializeField]Quaternion rotation;

		public Transformation(Transform transform) {
			position = transform.position;
			rotation = transform.rotation;
		}

		public void SetValue(Transform transform) {
			position = transform.position;
			rotation = transform.rotation;
		}

		public void SetLocalValue(Transform transform) {
			position = transform.localPosition;
			rotation = transform.localRotation;
		}

		public void SetTransform(Transform transform) {
			transform.position = position;
			transform.rotation = rotation;
		}

		public void SetLocalTransform(Transform transform) {
			transform.localPosition = position;
			transform.localRotation = rotation;
		}

		public Vector3 Position {
			get { return position; }
			set { position = value;}
		}

		public Quaternion Rotation {
			get { return rotation; }
			set { rotation = value;}
		}
	}

	// -------------------------------------------------------
	[SerializeField]Camera m_MainCamera = null;
	[SerializeField]Transform m_CameraRig = null;
	[SerializeField]Transform m_CameraPivot = null;
	[SerializeField]float m_TransitionSpeed = 0f;

	[SerializeField]float m_BlockWidth = 0f;
	[SerializeField]float m_BloderWidth = 0f;

	[SerializeField]float m_Fov = 0f;
	[SerializeField]float m_Near = 0f;
	[SerializeField]float m_Far = 0f;
	[SerializeField]float m_MaxSize = 0f;
	[SerializeField]float m_Size = 0;

	[SerializeField]float m_MapCameraViewAngle;

	Matrix4x4 m_OrthographicMatrix;
	Matrix4x4 m_PerspectiveMatrix;

	bool m_IsProjectionTranslating = false;
	bool m_IsPositionTranslating = false;

	float m_ProjectionTranslativeTime = 1;
	float m_Aspect;

	[SerializeField] Transformation m_Turret_Rig;
	[SerializeField] Transformation m_Turret_Pivot;
	[SerializeField] Transformation m_Turret_Camera;
	[SerializeField] Transformation m_Map_Rig;
	[SerializeField] Transformation m_Map_Pivot;
	[SerializeField] Transformation m_Map_Camera;

	Transform m_CameraTransform;

	const float k_MinSize = 5;
	const float k_MinAngle = 30f;
	const float k_MaxAngle = 90f;

	// -------------------------------------------------------
	void Awake()
	{
		m_CameraTransform = m_MainCamera.transform;
		m_Aspect = m_MainCamera.aspect;

		CalculateMatrices();
		m_MainCamera.projectionMatrix = m_OrthographicMatrix;
	}

	void Update()
	{
		float x = Input.GetAxis("Mouse X");
		float y = Input.GetAxis("Mouse Y");
		float scroll = Input.GetAxis("Mouse ScrollWheel");

		// If isn't in map view, or doesn't have movement, just return
		if (!GlobalVariable.Instance.IsMapView || (Mathf.Abs(x) + Mathf.Abs(y)< 0.1f && Mathf.Abs(scroll) < 0.1f)) return;

		if (Input.GetButton("Fire1")) {
			// Rotate rig by x
			// Rotate pivot by y
			m_CameraRig.Rotate(0, x, 0);
			m_CameraPivot.Rotate(-y, 0, 0);

			Vector3 angle = m_CameraPivot.localEulerAngles;
			if (angle.x + angle.y + angle.z > k_MaxAngle) angle.x = k_MaxAngle;
			if (angle.x < k_MinAngle) angle.x = k_MinAngle;
			m_CameraPivot.localEulerAngles = angle;
		}

		if (Input.GetButton("Fire2")) {
			Vector3 position = new Vector3(x, 0, y);
			position = m_CameraRig.TransformDirection(position);
			m_CameraRig.position += position;
		}

		if (Mathf.Abs(scroll) < 0.1f) return;
		scroll *= -3;
		m_Size += scroll;
		if (m_Size > m_MaxSize) m_Size = m_MaxSize;
		if (m_Size < k_MinSize) m_Size = k_MinSize;
		m_OrthographicMatrix = Matrix4x4.Ortho(-m_Size * m_Aspect, m_Size * m_Aspect, -m_Size, m_Size, m_Near, m_Far);
		m_MainCamera.projectionMatrix = m_OrthographicMatrix;
	}

	float CalculateMaxOrthoSize()
	{
		float blockDiagonalLength = Mathf.Sqrt(m_BlockWidth * m_BlockWidth * 2);
		float mapDiagonalWidth = (GlobalVariable.Instance.MapWidth + GlobalVariable.Instance.MapLength) * blockDiagonalLength * 0.25f + m_BloderWidth;
		float mapDiagonalHeight = mapDiagonalWidth * Mathf.Sin(40*Mathf.Deg2Rad);
		float orthoSize = mapDiagonalWidth / m_Aspect;
		m_MaxSize = mapDiagonalWidth;
		return orthoSize > mapDiagonalHeight ? orthoSize: mapDiagonalHeight;
	}

	float CalculateOrthoSize(float tiltAngle)
	{
		float mapDiagonalHeight = m_MaxSize * Mathf.Sin(tiltAngle * Mathf.Deg2Rad);
		float orthoSize = m_MaxSize / m_Aspect;
		return orthoSize > mapDiagonalHeight ? orthoSize: mapDiagonalHeight;
	}

	void CalculateMatrices()
	{
		m_Size = CalculateMaxOrthoSize();

		m_OrthographicMatrix = Matrix4x4.Ortho(-m_Size * m_Aspect, m_Size * m_Aspect, -m_Size, m_Size, m_Near, m_Far);
		m_PerspectiveMatrix = Matrix4x4.Perspective(m_Fov, m_Aspect, m_Near, m_Far);
	}

	public void MoveToTurretView(Transform turret)
	{
		// If camera is moving, or it's already in turret view, just return.
		if (m_IsPositionTranslating || !GlobalVariable.Instance.IsMapView) return;

		// Save curret transformation.
		SaveMapTransformation();

		// Save current position of camera.
		Transformation currentCameraTrans = new Transformation(m_CameraTransform);

		// Set whole camera rig to position
		m_Turret_Rig.Position = turret.position;
		m_Turret_Rig.SetTransform(m_CameraRig);
		m_Turret_Pivot.SetLocalTransform(m_CameraPivot);
		m_Turret_Camera.SetLocalTransform(m_CameraTransform);

		// Record current position of camera as target position
		Vector3 position = m_CameraTransform.position;
		Quaternion rotation = m_CameraTransform.rotation;

		// Set camera back to original position
		currentCameraTrans.SetTransform(m_CameraTransform);

		// Set projection mode.
		OrthographicMode = false;
		GlobalVariable.Instance.IsMapView = false;

		// Start moving.
		StartCoroutine(MoveCameraTo(position, rotation));
	}

	public void MoveToMapView()
	{
		// If camera is moving, or it's already in map view, just return.
		if (m_IsPositionTranslating || GlobalVariable.Instance.IsMapView) return;

		// Save current position of camera.
		Transformation currentCameraTrans = new Transformation(m_CameraTransform);

		// Set whole camera rig to position
		m_Map_Rig.SetTransform(m_CameraRig);
		m_Map_Pivot.SetLocalTransform(m_CameraPivot);
		m_Map_Camera.SetLocalTransform(m_CameraTransform);

		// Record current position of camera as target position
		Vector3 position = m_CameraTransform.position;
		Quaternion rotation = m_CameraTransform.rotation;

		// Set camera back to original position
		currentCameraTrans.SetTransform(m_CameraTransform);

		// Set projection mode.
		OrthographicMode = true;
		GlobalVariable.Instance.IsMapView = true;

		// Start moving.
		StartCoroutine(MoveCameraTo(position, rotation));
	}

	void SaveMapTransformation()
	{
		m_Map_Rig.SetValue(m_CameraRig);
		m_Map_Pivot.SetLocalValue(m_CameraPivot);
		m_Map_Camera.SetLocalValue(m_CameraTransform);
	}

	bool OrthographicMode
	{
		set {
			if (m_IsProjectionTranslating || GlobalVariable.Instance.IsMapView == value) return;
			StartCoroutine(ChangeProjectionMode(value));
		}
	}

	public bool isTranslating
	{
		get {
			return (m_IsPositionTranslating || m_IsProjectionTranslating);
		}
	}

	IEnumerator MoveCameraTo(Vector3 position, Quaternion rotation)
	{
		Vector3 originalPosition = m_CameraTransform.position;
		Quaternion originalRotation = m_CameraTransform.rotation;
		float time = 0;

		m_IsPositionTranslating = true;
		while (time < 1) {
			time += Time.deltaTime * m_TransitionSpeed;
			Mathf.Clamp(time,0,1);
			m_CameraTransform.position = Vector3.Lerp(originalPosition, position, time);
			m_CameraTransform.rotation = Quaternion.Slerp(originalRotation, rotation, time);
			yield return null;
		}
		m_IsPositionTranslating = false;
	}

	IEnumerator ChangeProjectionMode(bool mode)
	{
		if ((mode && m_ProjectionTranslativeTime >= 1) 
		|| (!mode && m_ProjectionTranslativeTime <= 0)) {
			m_MainCamera.orthographic = mode;
			yield break;
		}

		m_IsProjectionTranslating = true;
		while (mode?
		(m_ProjectionTranslativeTime < 1): 
		(m_ProjectionTranslativeTime > 0)) {
			m_MainCamera.orthographic = true;
			m_ProjectionTranslativeTime += Time.deltaTime * m_TransitionSpeed * (mode?1:-1);
			m_MainCamera.projectionMatrix = MatrixLerp(m_PerspectiveMatrix, m_OrthographicMatrix, m_ProjectionTranslativeTime);
			yield return null;
		}
		m_MainCamera.orthographic = mode;
		m_IsProjectionTranslating = false;
	}

	static Matrix4x4 MatrixLerp(Matrix4x4 from, Matrix4x4 to, float time)
	{
		if (time <= 0) return from;
		if (time >= 1) return to;
		Matrix4x4 matrix = new Matrix4x4();
		for (int i = 0; i < 16; i ++) {
			matrix[i] = Mathf.Lerp(from[i], to[i], Logistic(time));
		}
		return matrix;
	}

	// Use for smooth the transition.
	static float Logistic(float time)
	{
		return (2.0f/(1+Mathf.Exp(-8*time)))-1;
	}
}
