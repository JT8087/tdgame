﻿using UnityEngine;
using System.Collections;

namespace TDGame.ObjectPooling
{
	public interface IPoolableObject {
		bool isIdle();
		void reset();
		void recycle();
		void register(ObjectPool pool, int id);
	}
}
