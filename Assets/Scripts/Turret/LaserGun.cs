﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TDGame.Enemys;

namespace TDGame.Turrets
{
	public class LaserGun : Turret
	{
		[Header("Laser Turret Settings")]
		[SerializeField] EffectGroup m_HittingEffect = null;
		[SerializeField] Light m_FireLighting = null;

		LineRenderer m_Laser;

		// -------------------------------------------------------------------------
		// Unity event funcitons
		void Start()
		{
			// Setting the SphereCollider to detecter
			m_Detecter = GetComponentInChildren<SphereCollider>();
			m_Detecter.radius = m_Range;
			m_Detecter.isTrigger = true;

			m_Laser = GetComponent<LineRenderer>();
			m_Laser.enabled = false;

			ceaseShooting();
		}

		void Update()
		{
			if (m_Overriding) return;
			// Pick up a target
			// If can't pick a target, stop here.
			if (!PickTarget()) {
				ceaseShooting();
				return;
			}
			// Get the target position.
			Vector3 targetPosition = m_Target.transform.position + m_Target.m_HitOffset;
			// ...take aim...
			Aiming(targetPosition);
			// ... and shoot
			Shoot();
		}

		// ------------------------------------------------------------------

		/// <summary>
		/// Shoot a target.
		/// </summary>
		protected override void Shoot()
		{
			RaycastHit hit;

			// If have a target...
			if (m_Target != null
				
			// If hit something...
			 && Physics.Raycast(m_FireTransform.position, 
								m_FireTransform.forward, 
								out hit, 
								m_Range, 
								LayerMask.GetMask(m_AttackHitLayer))
				
			// If the thing we hit is an enemy, start shooting
			&& hit.transform.CompareTag("Enemy")) {
				startShooting(hit);
			} else { // If not, cease shooting
				ceaseShooting();
			}
		}

		public override void Override(Ray direction, bool attack)
		{
			RaycastHit hit;

			// See what the camera currently pointing at.
			Physics.Raycast(
				direction, out hit, Mathf.Infinity, 
				LayerMask.GetMask(m_AttackHitLayer));

			// If there's nothing, make up a target.
			if (hit.transform == null) hit.point = direction.GetPoint(100);

			// Turn turret to target
			Aiming(hit.point);

			// If want to open fire
			if (attack) {
				// Get barrel direction
				direction.origin = m_FireTransform.position;
				direction.direction = m_FireTransform.forward;

				// See if it can hit something
				Physics.Raycast(
					direction, out hit, Mathf.Infinity, 
					LayerMask.GetMask(m_AttackHitLayer));

				// If hit nothing, make up a hitting point.
				if (hit.transform == null) hit.point = direction.GetPoint(100);

				// Start shooting
				startShooting(hit);
			} else {
				// If don't fire, cease possible previous shooting.
				ceaseShooting();
			}
		}

		void startShooting(RaycastHit hit)
		{
			// Enable fire effect
			m_FireLighting.enabled = true;

			// Enable laser effect.
			m_Laser.SetPosition(0, m_FireTransform.position);
			m_Laser.SetPosition(1, hit.point);
			m_Laser.enabled = true;

			// if did't actually hit something, return here.
			if (hit.transform == null) {
				m_HittingEffect.enabled = false;
				return;
			}

			// Setting hitting effect.
			m_HittingEffect.transform.position = hit.point;
			m_HittingEffect.transform.forward = hit.normal;
			m_HittingEffect.enabled = true;

			// Damage target.
			Enemy enemy = hit.transform.GetComponent<Enemy>();

			if (enemy != null) {
				enemy.gotHit(m_Damage * Time.deltaTime);
				if (enemy.isDead()) m_Targets.Remove(enemy);
			}
		}

		void ceaseShooting()
		{
			// Disable effects
			m_Laser.enabled = false;
			m_FireLighting.enabled = false;
			m_HittingEffect.enabled = false;
		}
	}
}