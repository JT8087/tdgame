﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class DataBaseCollector : MonoBehaviour
{
	public static DataBaseList EnemyDataBase;
	public static DataBaseList TurretDataBase;

	// Use this for initialization
	void Awake ()
	{
		if (true)
		{
			#if UNITY_EDITOR
			object getDataBase = Resources.Load("EnemyDatabase");
			if (getDataBase == null) {
				EnemyDataBase = CreateDatabase(EnemyDataBase, "EnemyDatabase");
			} else {
				EnemyDataBase = Resources.Load<DataBaseList>("EnemyDatabase");
			}

			getDataBase = Resources.Load("TurretDataBase");
			if (getDataBase == null) {
				TurretDataBase = CreateDatabase(TurretDataBase, "TurretDataBase");
			} else {
				TurretDataBase = Resources.Load<DataBaseList>("TurretDataBase");
			}
				
			#else
			EnemyDataBase = Resources.Load<DataBaseList>("EnemyDatabase");
			TurretDataBase = Resources.Load<DataBaseList>("TurretDataBase");
			#endif

			EnemyDataBase.uniquifyID();
			TurretDataBase.uniquifyID();
		}
	}

	#if UNITY_EDITOR
	DataBaseList CreateDatabase(DataBaseList database, string dataBaseName)
	{
		database = ScriptableObject.CreateInstance<DataBaseList>();
		string path = "Assets/Resources/" + dataBaseName + ".asset";
		AssetDatabase.CreateAsset(database, path);
		AssetDatabase.SaveAssets();
		database.DataList.Add(new DataBaseSturt());
		return database;
	}
	#endif
}
