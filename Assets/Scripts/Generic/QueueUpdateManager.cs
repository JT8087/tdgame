﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QueueUpdateManager : MonoBehaviour
{
	public const int MaxQueue = 8;
	public delegate void QueueUpdateDelegate();
//	public static QueueUpdateDelegate[] QueueAwakeList 			= new QueueUpdateDelegate[MaxQueue];
//	public static QueueUpdateDelegate[] QueueStartList 			= new QueueUpdateDelegate[MaxQueue];
	public static QueueUpdateDelegate[] QueueUpdateList 		= new QueueUpdateDelegate[MaxQueue];
	public static QueueUpdateDelegate[] QueueFixedUpdateList 	= new QueueUpdateDelegate[MaxQueue];
	public static QueueUpdateDelegate[] QueueLateUpdateList 	= new QueueUpdateDelegate[MaxQueue];
	public static QueueUpdateManager Manager;

	void Awake()
	{
		if (FindObjectsOfType<QueueUpdateManager>().Length > 1)
		{
			Debug.LogWarning("There cannot be more than one QueueUpdateManager!!!");
			Destroy(gameObject);
		}
		Manager = this;
	}

	void Update()
	{
		for (int i = 0; i < MaxQueue; i ++)
		{
			if (QueueUpdateList[i] != null)
				QueueUpdateList[i]();
		}
	}

	void FixedUpdate()
	{
		for (int i = 0; i < MaxQueue; i ++)
		{
			if (QueueFixedUpdateList[i] != null)
				QueueFixedUpdateList[i]();
		}
	}

	void LateUpdate()
	{
		for (int i = 0; i < MaxQueue; i ++)
		{
			if (QueueLateUpdateList[i] != null)
				QueueLateUpdateList[i]();
		}
	}
}
