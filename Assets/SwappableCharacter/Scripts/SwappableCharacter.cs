﻿using UnityEngine;
using System.Collections;

public class SwappableCharacter : MonoBehaviour {
	[MinMaxSlider(0, 20)][SerializeField] Vector2 m_RandomMoveDistance = new Vector2(3, 6);
	[MinMaxSlider(0,  5)][SerializeField] Vector2 m_RandomMoveSpeed = new Vector2(1, 3);
	[MinMaxSlider(0,  5)][SerializeField] Vector2 m_RandomIdleTime = new Vector2(1, 3);

	bool m_IsOverriding;	// Is this character in player's control?
	bool m_IsMoving;		// Is this character in moving?

	float m_CurrentSpeed;	// Current moving speed and turing speed.
	float m_LastIdleTime;	// How long this character need to be waiting.
	float m_LastDistance;	// How far this character need to be moving.
	Vector3 m_CurrentDirection;	// Where does this character heading to

	//-----------------------------------------------------------------------------
	// Unity functions

	void Awake() {
		ResetValues();
		GenerateRandomData();
	}

	void Update() {
		// If is in player control, quit.
		// Otherwise, wandering around.
		if (m_IsOverriding) return;
		else WanderingAround();
	}

	//-----------------------------------------------------------------------------
	// Movement

	// Move character to a direction.
	void MoveToward(Vector3 direction) {
		Vector3 movement = direction * Time.deltaTime * m_CurrentSpeed;
		transform.Translate(movement, Space.World);
	}

	// Rotate character to a direction
	void RotateToward(Vector3 direction) {
		transform.forward = Vector3.RotateTowards(transform.forward, direction, m_CurrentSpeed * Time.deltaTime * 2, 0);
	}

	//-----------------------------------------------------------------------------
	// Randomize(AI control)

	void WanderingAround() {
		// If this character is in moving state.
		if (m_IsMoving)
		{
			// Move character
			MoveToward(m_CurrentDirection);
			RotateToward(m_CurrentDirection);

			// Counting how far this character still need to go.
			m_LastDistance -= m_CurrentSpeed * Time.deltaTime;

			// If this character has moved far enough, turn off moving state.
			if (m_LastDistance <= 0) {
				m_IsMoving = false;
			}
		}	// If not in moving state.
		else {
			// Counting how long this character still need to wait.
			m_LastIdleTime -= Time.deltaTime;

			// If this character has waited long enough, turn on moving state.
			if (m_LastIdleTime <= 0){
				m_IsMoving = true;
				GenerateRandomData();
			}
		}
	}

	// Generate random movement.
	void GenerateRandomData() {
		float baseRan = Random.value;
		m_LastDistance = Mathf.Lerp(m_RandomMoveDistance.x, m_RandomMoveDistance.y, baseRan);
		m_CurrentSpeed = Mathf.Lerp(m_RandomMoveSpeed.x, m_RandomMoveSpeed.y, baseRan);
		m_LastIdleTime = Mathf.Lerp(m_RandomIdleTime.x, m_RandomIdleTime.y, baseRan);
		m_CurrentDirection = GenerateRandomDirection();
	}

	// Generate a random direction.
	Vector3 GenerateRandomDirection() {
		Vector2 random = Random.insideUnitCircle;
		Vector3 direction = new Vector3(random.x, 0, random.y);
		return direction;
	}

	//-----------------------------------------------------------------------------
	// Overriding(Player control)

	public void StartOverride() {
		ResetValues();
		m_IsOverriding = true;
	}

	public void QuitOverride() {
		GenerateRandomData();
		m_IsOverriding = false;
	}

	public void Overriding(Vector3 direction, Vector3 movement) {
		if (!m_IsOverriding) {
			Debug.LogWarning("Some script try to control " + gameObject.name + " but this character isn't set to overriding yet!");
		}
		direction.y = 0;
		movement.y = 0;
		m_CurrentSpeed = movement.magnitude * m_RandomMoveSpeed.y;
		if (m_CurrentSpeed > 0.1f) {
			MoveToward(movement);
		}
		RotateToward(direction);
	}

	//-----------------------------------------------------------------------------
	// Reset values.
	void ResetValues() {
		m_IsOverriding = false;
		m_IsMoving = true;

		m_CurrentSpeed = 1;
		m_LastIdleTime = 0;
		m_LastDistance = 0;
		m_CurrentDirection = Vector3.forward;
	}
}
