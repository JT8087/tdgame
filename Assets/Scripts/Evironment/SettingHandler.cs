﻿using UnityEngine;
using System.Collections;
using System.Reflection;

[ExecuteInEditMode]
public class SettingHandler : MonoBehaviour {
	public Transform m_Camera;
	public Transform m_Pivot;
	public Transform m_Rig;
	public CameraTransitionHandler m_Handler;

	public bool SetMapPosition = false;
	public bool SetTurretPosition = false;

	void Update () {
		if (SetMapPosition) {
//			m_Handler.m_Map_Camera.SetLocalValue(m_Camera);
//			m_Handler.m_Map_Pivot.SetLocalValue(m_Pivot);
//			m_Handler.m_Map_Rig.SetValue(m_Rig);
			SetMapPosition = false;
		}

		if (SetTurretPosition) {
//			m_Handler.m_Turret_Camera.SetLocalValue(m_Camera);
//			m_Handler.m_Turret_Pivot.SetLocalValue(m_Pivot);
//			m_Handler.m_Turret_Rig.SetValue(m_Rig);
			SetTurretPosition = false;
		}
	}
}
