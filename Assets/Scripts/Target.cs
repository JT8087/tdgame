﻿using UnityEngine;
using System.Collections;

public class Target : MonoBehaviour {
	public float m_MoveSpeed;
	public float m_SpinSpeed;
	public bool m_Move;
	public bool m_Spin;
	public Vector3 m_Movement;
	public Vector3 m_Rotation;

	void Update() {
		if (m_Move) {
			transform.Translate(m_Movement * Time.deltaTime * m_MoveSpeed);
		}

		if (m_Spin) {
			transform.Rotate(m_Rotation * Time.deltaTime * m_SpinSpeed);
		}
	}
}
