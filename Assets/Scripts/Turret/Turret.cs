﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TDGame.Enemys;

namespace TDGame.Turrets
{
	public abstract class Turret : MonoBehaviour
	{
		[Header("General Turret Settings")]
		public bool m_Overriding = false;

		[SerializeField]protected float m_Price = 100.0f;
		[SerializeField]protected float m_TurningSpeed = 200.0f;
		[SerializeField]protected float m_Range = 10.0f;
		[SerializeField]protected float m_Damage = 10.0f;
		[SerializeField]protected Transform m_TurrentPivot = null;
		[SerializeField]protected Transform m_BarrelPivot = null;
		[SerializeField]protected Transform m_FireTransform = null;
		[SerializeField]protected string[] m_AttackHitLayer = null;

		protected Enemy m_Target;
		protected List<Enemy> m_Targets = new List<Enemy>();
		protected SphereCollider m_Detecter;

		abstract protected void Shoot();

		abstract public void Override(Ray target, bool attack);

		// -------------------------------------------------------------------------
		// Unity event funcitons
		protected void Awake() {
			// Setting detecter
			m_Detecter = GetComponentInChildren<SphereCollider>();
			m_Detecter.radius = m_Range;
			m_Detecter.isTrigger = true;

			// Check target list and clear invalid target.
			InvokeRepeating("clearList", 3, 3);
		}

		void OnTriggerEnter(Collider other) {
			if (other.CompareTag("Enemy")) {
				m_Targets.Add(other.gameObject.GetComponent<Enemy>());
			}
		}

		void OnTriggerExit(Collider other) {
			if (other.CompareTag("Enemy"))
				m_Targets.Remove(other.gameObject.GetComponent<Enemy>());
		}

		//------------------------------------------------------------------------------
		// Protected functions

		/// <summary>
		/// Aiming an target.
		/// </summary>
		protected virtual void Aiming(Vector3 targetPos) {
			// If have no turrent pivot, just return.
			if (m_TurrentPivot == null) return;

			// Diretion from turret to target.
			Vector3 dir = targetPos - m_TurrentPivot.position;

			// Transform this direction to local space.
			dir = m_TurrentPivot.InverseTransformDirection(dir);

			// Calculate angle and turning turret
			float angle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg;
			m_TurrentPivot.Rotate(Vector3.up, Mathf.Clamp(angle, -m_TurningSpeed * Time.deltaTime, m_TurningSpeed * Time.deltaTime));

			// If have no barrel pivot,  return here.
			if (m_BarrelPivot == null) return;

			// Diretion from barrel to target.
			dir = targetPos - m_BarrelPivot.position;

			// Current barrel direction.
			Vector3 turretDir = m_BarrelPivot.forward;

			// Calculate angle and turning barrel
			angle = Vector3.Angle(dir, Vector3.up) - Vector3.Angle(turretDir, Vector3.up);
			m_BarrelPivot.Rotate(Vector3.right, Mathf.Clamp(angle, -m_TurningSpeed * Time.deltaTime, m_TurningSpeed * Time.deltaTime));
		}

		protected virtual bool PickTarget() {
			m_Target = null;
			for (int i = 0; i < m_Targets.Count && m_Target == null; i ++) {
				m_Target = m_Targets[i];

				// If target is inactivated or without range, skip it.
				if (m_Target.isDead() 
					|| !isWithInRange(m_Targets[i].transform.position)) {
					m_Target = null;
				}
			}

			return m_Target == null? false: true;
		}

		protected virtual void clearList() {
			int cleared = 0;
			for (int i = 0; i < m_Targets.Count - cleared; i ++) {
				// Clear inactivated enemy.
				if (m_Targets[i].isDead()
					|| !isWithInRange(m_Targets[i].transform.position)) {
					m_Targets.RemoveAt(i);
					cleared ++;
					i --;
					continue;
				}

				// Clear references to same object.
				for (int j = i + 1; j < m_Targets.Count - cleared; j ++) {
					if (ReferenceEquals(m_Targets[i], m_Targets[j])) {
						m_Targets.RemoveAt(j);
						cleared ++;
						j --;
					}
				}
			}
		}

		protected virtual bool isWithInRange(Vector3 target) {
			Vector3 distance = target - transform.position;
			return distance.sqrMagnitude < m_Range * m_Range;
		}
	}
}
