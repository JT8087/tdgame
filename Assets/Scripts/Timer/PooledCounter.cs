﻿//using UnityEngine;
//using System.Collections;
//
//namespace TDGame.Timer
//{
//	public class PooledCounter : Counter
//	{
//		static TimerPool m_Pool;
//
//		/// <summary>
//		/// Get a new timer from TimerPool.
//		/// </summary>
//		public static PooledCounter create(float countTime, bool repeat = false)
//		{
//			TimerPool.loadPool();
//			PooledCounter counter = TimerPool.create<PooledCounter>();
//			counter.CountDownTime = countTime;
//			counter.repeat = repeat;
//			return counter;
//		}
//
//		public void recycle()
//		{
//			TimerPool.recycle(this);
//		}
//	}
//}
