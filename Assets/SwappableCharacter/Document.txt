Swappable Character

Install:
	Copy the folder(SwappableCharacter) into your Assets folder.
	
Requirement:
	Basic Unity project input setting. This using Horizontal, Vertical, Mouse X, and Mouse Y. If you have changed one of those you may need to change some codes as well.
	
How to use:
	There are two scripts inside script folder(SwappableCharacter/Scripts), SwappableCharacter and PlayerControlCamera. SwappableCharacter is use to control characters, and PlayerControlCamera is for player control and move camera.
	
	To use this, attach SwappableCharacter to your characters. PlayerControlCamera requires a pivot GameObject to work. To do that, simple create an empty GameObject in your Inspector, and move your main camera into it. After that you can place PlayerControlCamera on pivot or wherever you like.
	
	When all of above are set, you can just go straight into play mode. you should see your camera is attached to one of the characters in scene. You can specify which character it will attach when starting play by drag the character into Controlled Character in Inspector.
	
	You can move around by wasd or arrow keys and look around by move your mouse. Press E will move camera to the character you're pointing at and control it. Press C will switch cursor's appearance.
	
	For more detail informations about adjustable properties and public functions and such, please read below.
	
	// ----------------- More Detail Informations -----------------
	// ------SwappableCharacter------
	
	SwappableCharacter:
		SwappableCharacter handle character's movement. When player isn't in control of a character, it will make character randomly wandering around.
		Noted: This script doesn't consider about complex movement and physics simulation.
		
		Adjustable properties in inspector:
			Random Move Distance:	How far this character will move each time. In meters by default.
			Random Move Speed:		How fast this character will move each time. In seconds by default.
			Random Idle Time:		How long this character will wait each time. In seconds by default.
			
		Public functions
			void StartOverride():
				Tell this character stop it's movement and let player control it.
				
			void QuitOverride():
				Tell this character player is no longer control it and back to wandering.
				
			void Overriding(Vector3 direction, Vector3 movement):
				Sending moving direction and movement to this character. both direction and movement is in world space.
				
				
	// ------PlayerControlCamera------
	
	PlayerControlCamera:
		PlayerControlCamera make player can control characters and able to move.
		
		Adjustable properties in inspector:
			Lock Cursor In Start:	Lock or unlock cursor when game start, note it only effective when you just start play.
			Tilt Angle Limit:		How high and low can camera tilt.
			Mouse Sensitiveness:	How fast will viewpoint move by mouse
			Camera Transfer Speed:	How fast will the transfer go. For example, 1 means a second, 2 means half a second
			Main Camera:			The camera does this script will affect, you can leave it empty as it can find the main camera in scene itself.
			Controlled Character:	The character player controlling, you can leave it empty when start to play too.
			
		Public variables:
			bool LockCursor:		Lock or unlock cursor.
		
		
Ps: This is pretty much my first time writing a document(not to mention writing it in English), so if you'd seen any incorrect, unclear, unspecified place(I'm sure there is plenty of them) please point that out to me, I will see what I can do to improve in the future.