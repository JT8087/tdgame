﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TDGame.TimerManagement {
	public class TimerManager : MonoBehaviour {
		static TimerManager _instance;
		public List<Timer> m_TimerList;

		void Awake() {
			m_TimerList = new List<Timer>();
		}

		void Update() {
			for (int i = 0; i < m_TimerList.Count; i ++) {
				if (ReferenceEquals(m_TimerList[i], null)) continue;
				m_TimerList[i]._CountDownTime(Time.deltaTime);
				m_TimerList[i]._IdleTime(Time.deltaTime);
			}
		}

		public static TimerManager Instance {
			get {
				if (_instance == null) {
					_instance = FindObjectOfType<TimerManager>();
				}
				if (_instance == null) {
					GameObject timeCounter = new GameObject("TimerManager");
					_instance = timeCounter.AddComponent<TimerManager>();
				}

				return _instance;
			}
		}

		public int Login(Timer timer) {
			for (int i = 0; i < m_TimerList.Count; i ++) {
				if (ReferenceEquals(m_TimerList[i], null)) {
					m_TimerList[i] = timer;
					return i;
				}
			}
			m_TimerList.Add(timer);
			return m_TimerList.Count - 1;
		}

		public void Logout(int ID) {
			if (ID < 0) return;
			m_TimerList[ID] = null;
		}
	}
}