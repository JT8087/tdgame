﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class VectorLearning : MonoBehaviour {

	enum VectorComputeDisplay
	{
		Angle,
		Angle_C,
		Cross,
		Dot,
		Lerp,
		MoveTowards,
		Project,
		ProjectOnPlane,
		Reflect,
		RotateTowards,
		Scale,
		Slerp,
		SmoothDamp,
	}

	public Vector3 outVector = Vector3.zero;
	public float outNumber = 0;

	[SerializeField] Transform m_Object1;
	[SerializeField] Transform m_Object2;
	[SerializeField] Transform m_Object3;
	[SerializeField] Vector3 m_Vector1;
	[SerializeField] Vector3 m_Vector2;
	[SerializeField] Vector3 m_Vector3;
	[SerializeField] float m_Parameter1;
	[SerializeField] float m_Parameter2;
	[SerializeField] float m_Parameter3;
	[SerializeField] Text m_Display;
	[SerializeField] VectorComputeDisplay m_Compute;
	[SerializeField] bool m_SwapVector1AndVector2;
	[SerializeField] bool m_SwapVector2AndVector3;
	[SerializeField] bool m_SwapVector1AndVector3;
	[SerializeField] bool m_SetVector1ToOutVector;
	[SerializeField] bool m_SetVector2ToOutVector;
	[SerializeField] bool m_SetVector3ToOutVector;
	[SerializeField] bool m_NormalizeVector1;
	[SerializeField] bool m_NormalizeVector2;
	[SerializeField] bool m_NormalizeVector3;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
	{
//		Vector3 outVector = Vector3.zero;
//		float 	outNumber = 0;

		if (m_Object1 != null)
		{
			m_Vector1 = m_Object1.position - transform.position;
		}

		if (m_Object2 != null)
		{
			m_Vector2 = m_Object2.position - transform.position;
		}

		if (m_Object3 != null)
		{
			m_Vector3 = m_Object3.position - transform.position;
		}

		boolControl();

		switch (m_Compute)
		{
			case VectorComputeDisplay.Angle:			outNumber = Vector3.Angle(m_Vector1, m_Vector2); break;
			case VectorComputeDisplay.Angle_C:			outNumber = angle(m_Vector1, m_Vector2); break;
			case VectorComputeDisplay.Cross:			outVector = Vector3.Cross(m_Vector1, m_Vector2); break;
			case VectorComputeDisplay.Dot:				outNumber = Vector3.Dot(m_Vector1, m_Vector2);break;
			case VectorComputeDisplay.Lerp:				outVector = Vector3.Lerp(m_Vector1, m_Vector2, m_Parameter1); break;
			case VectorComputeDisplay.MoveTowards: 		outVector = Vector3.MoveTowards(m_Vector1, m_Vector2, m_Parameter1); break;
			case VectorComputeDisplay.Project:			outVector = Vector3.Project(m_Vector1, m_Vector2); break;
			case VectorComputeDisplay.ProjectOnPlane: 	outVector = Vector3.ProjectOnPlane(m_Vector1, m_Vector2); break;
			case VectorComputeDisplay.Reflect: 			outVector = Vector3.Reflect(m_Vector1, m_Vector2); break;
			case VectorComputeDisplay.RotateTowards: 	outVector = Vector3.RotateTowards(m_Vector1, m_Vector2, m_Parameter1, m_Parameter2); break;
			case VectorComputeDisplay.Scale: 			outVector = Vector3.Scale(m_Vector1, m_Vector2); break;
			case VectorComputeDisplay.Slerp: 			outVector = Vector3.Slerp(m_Vector1, m_Vector2, m_Parameter1); break;
			case VectorComputeDisplay.SmoothDamp:		outVector = Vector3.SmoothDamp(m_Vector1, m_Vector2, ref m_Vector3, m_Parameter1, m_Parameter2, m_Parameter3);break;
		}

		Debug.DrawRay(transform.position, transform.InverseTransformDirection(m_Vector1));
		Debug.DrawRay(transform.position, outVector, Color.red);
		Debug.DrawRay(transform.position, m_Vector1, Color.green);
		Debug.DrawRay(transform.position, m_Vector2, Color.blue);
		Debug.DrawRay(transform.position, m_Vector3, Color.yellow);
		m_Display.text = 
			"\nOutVector: " + outVector + "\tLength: " + outVector.magnitude +
			"\nOutNumber: " + outNumber +
			"\nVector 1: " + m_Vector1 + "\tLength: " + m_Vector1.magnitude +
			"\nVector 2: " + m_Vector2 + "\tLength: " + m_Vector2.magnitude +
			"\nVector 3: " + m_Vector3 + "\tLength: " + m_Vector3.magnitude +
			"\nParameter1: " + m_Parameter1 +
			"\nParameter2: " + m_Parameter2 +
			"\nParameter3: " + m_Parameter3;
	}


	void boolControl()
	{
		if (m_SetVector1ToOutVector)
		{
			m_Vector1 = outVector;
		}

		if (m_SetVector2ToOutVector)
		{
			m_Vector2 = outVector;
		}

		if (m_SetVector3ToOutVector)
		{
			m_Vector3 = outVector;
		}

		if (m_SwapVector1AndVector2)
		{
			Vector3 temp = m_Vector1;
			m_Vector1 = m_Vector2;
			m_Vector2 = temp;
		}

		if (m_SwapVector2AndVector3)
		{
			Vector3 temp = m_Vector3;
			m_Vector3 = m_Vector2;
			m_Vector2 = temp;
		}

		if (m_SwapVector1AndVector3)
		{
			Vector3 temp = m_Vector1;
			m_Vector1 = m_Vector3;
			m_Vector3 = temp;
		}

		if (m_NormalizeVector1)
		{
			m_Vector1.Normalize();
		}

		if (m_NormalizeVector2)
		{
			m_Vector2.Normalize();
		}

		if (m_NormalizeVector3)
		{
			m_Vector3.Normalize();
		}

		if (m_Object1 != null)
		{
			m_Object1.position = transform.position + m_Vector1;
		}

		if (m_Object2 != null)
		{
			m_Object2.position = transform.position + m_Vector2;
		}

		if (m_Object3 != null)
		{
			m_Object3.position = transform.position + m_Vector3;
		}

		m_SetVector1ToOutVector = false;
		m_SetVector2ToOutVector = false;
		m_SetVector3ToOutVector = false;
		m_SwapVector1AndVector2 = false;
		m_SwapVector2AndVector3 = false;
		m_SwapVector1AndVector3 = false;
		m_NormalizeVector1 = false;
		m_NormalizeVector2 = false;
		m_NormalizeVector3 = false;
	}

	float angle(Vector3 a, Vector3 b)
	{
		float dot = a.x * b.x + a.z * b.z;
		float al = Mathf.Sqrt(a.x*a.x+a.z*a.z);
		float bl = Mathf.Sqrt(b.x*b.x+b.z*b.z);
		float cosr = dot / al / bl;
		return Mathf.Acos(cosr) * Mathf.Rad2Deg * Mathf.Sign(cross(a,b));
	}

	float cross(Vector3 a, Vector3 b)
	{
		return b.x*a.z - a.x*b.z;
	}
}
