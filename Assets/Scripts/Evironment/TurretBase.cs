﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using TDGame.Turrets;

public class TurretBase : MonoBehaviour, IPointerClickHandler {
	public bool m_HaveTurret = false;
	public Color m_SelectedColor;
	public float m_FlashRate = 1;
	public Vector3 m_BuildingOffset;

	Material m_Material;
	Color m_OriginalColor;
	Turret m_Turret;

	void Awake()
	{
		m_Material = GetComponent<MeshRenderer>().material;
		m_OriginalColor = m_Material.GetColor("_EmissionColor");
	}

	public void BuildingTurret(int turretID)
	{
		if (m_HaveTurret) return;
		m_HaveTurret = true;
		GameObject turret = DataBaseCollector.TurretDataBase.GetData(turretID);
		turret = GameObject.Instantiate(turret, transform.position + m_BuildingOffset, transform.rotation) as GameObject;
		m_Turret = turret.GetComponent<Turret>();
		turret.transform.parent = transform;
	}

	public void RemoveTurret()
	{
		if (!m_HaveTurret) return;
		m_HaveTurret = false;

		Destroy(m_Turret.gameObject);
	}
		
	IEnumerator FlashColor()
	{
		float time = 0;
		while (time < 1) {
			m_Material.SetColor("_EmissionColor", Color.Lerp(m_OriginalColor, m_SelectedColor, time));
			time += Time.deltaTime * m_FlashRate * 2;
			yield return null;
		}

		time = 0;
		while (time < 1) {
			m_Material.SetColor("_EmissionColor", Color.Lerp(m_SelectedColor, m_OriginalColor, time));
			time += Time.deltaTime * m_FlashRate * 2;
			yield return null;
		}

		m_Material.SetColor("_EmissionColor", m_OriginalColor);
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (!GlobalVariable.Instance.IsMapView) return;
		print(Input.mousePosition);
		//BuildMenu.Current.MoveInPosition(Input.mousePosition);

		if (!m_HaveTurret) {
			if (eventData.button == PointerEventData.InputButton.Left)
				BuildingTurret(1);
			else
				BuildingTurret(0);
		} else {
			RemoveTurret();
		}

		StartCoroutine(FlashColor());
	}
}
