﻿//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//
//namespace TDGame.Timer
//{
//	public class Timer
//	{
//		protected bool counting;
//		protected bool activated;
//		protected float passedTime;
//
//		public Timer()
//		{
//			counting  = false;
//			activated = false;
//			passedTime = 0;
//		}
//
//		// ----------------------------------------------------------------------
//		// Apis
//
//		/// <summary>
//		/// Activate this timer.
//		/// </summary>
//		public virtual bool activate
//		{
//			get { return activated; }
//			set {
//				if (value) {
//					activated = true;
//					counting  = true;
//				} else {
//					activated = false;
//					counting  = false;
//				}
//				passedTime = 0;
//			}
//		}
//
//		/// <summary>
//		/// Sets a value indicating whether this <see cref="TDGame.Timer.Timer"/> is pause.
//		/// </summary>
//		/// <value><c>true</c> if pause; otherwise, <c>false</c>.</value>
//		public virtual bool pause
//		{
//			get {
//				isAbleToAccess();
//				return !counting;
//			}
//			set {
//				if (!isAbleToAccess()) return;
//				counting = !value;
//			}
//		}
//
//		/// <summary>
//		/// Reset timing
//		/// </summary>
//		/// <param name="tag">Tag.</param>
//		public virtual void reset()
//		{
//			// If this timer isn't able to access, just return.
//			if (!isAbleToAccess()) return;
//			passedTime = 0;
//		}
//
//		/// <summary>
//		/// Return how much time has passed.
//		/// </summary>
//		/// <returns>The time.</returns>
//		/// <param name="tag">Tag.</param>
//		public virtual float PassedTime
//		{
//			get {
//				// If this timer isn't able to access, return -1.
//				if (!isAbleToAccess()) return -1;
//				return passedTime;
//			}
//		}
//
//		/// <summary>
//		/// (DONT USE MANUALLY!!!) Timing the specified deltatime. 
//		/// </summary>
//		/// <param name="deltatime">Deltatime.</param>
//		public virtual void timing(float deltatime)
//		{
//			passedTime += deltatime;
//		}
//
//		// ----------------------------------------------------------------------
//		// Internal method
//
//		/// <summary>
//		/// Is this timer able to access.
//		/// </summary>
//		/// <returns><c>true</c>, if activate was ised, <c>false</c> otherwise.</returns>
//		protected virtual bool isAbleToAccess()
//		{
//			if (!activated) {
//				Debug.LogWarning("Try to access a not activated timer!!!");
//				return false;
//			}
//
//			return true;
//		}
//	}
//}
