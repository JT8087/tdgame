﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BuildMenu : MonoBehaviour {
	public static BuildMenu Current;
	RectTransform m_Rect;

	void Awake() {
		Current = this;
		m_Rect = GetComponent<RectTransform>();
		transform.parent.GetComponent<CanvasScaler>().referenceResolution = new Vector2(Screen.width, Screen.height);
		transform.parent.GetComponent<RectTransform>().localScale = Vector3.one;
	}

	public void MoveInPosition(Vector3 position) {
		Vector2 pos = new Vector2(position.x, position.y);
		m_Rect.anchoredPosition = pos;
	}
}
