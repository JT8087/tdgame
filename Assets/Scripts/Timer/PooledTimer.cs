﻿//using UnityEngine;
//using System.Collections;
//
//namespace TDGame.Timer
//{
//	public class PooledTimer : Timer
//	{
//		static TimerPool m_Pool;
//
//		/// <summary>
//		/// Get a new timer from TimerPool.
//		/// </summary>
//		public static PooledTimer create()
//		{
//			TimerPool.loadPool();
//			return TimerPool.create<PooledTimer>();
//		}
//			
//		public void recycle()
//		{
//			TimerPool.recycle(this);
//		}
//	}
//}