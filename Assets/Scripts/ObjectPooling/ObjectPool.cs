﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TDGame.ObjectPooling
{
	public class ObjectPool : Object
	{
		GameObject _Prefab;
		[SerializeField]List<GameObject> m_ObjectPool = new List<GameObject>();
		[SerializeField]List<IPoolableObject> m_ObjectInterfaces = new List<IPoolableObject>();
		[SerializeField]Queue<int> m_ReadyToUse = new Queue<int>();

		public int m_DefPooledAmount = 4;
		public int m_MaxPooledAmount = 16;
		public bool m_GrowOutAble = true;

		public ObjectPool(GameObject prefab) {
			Prefab = prefab;
			for (int i = 0; i < m_DefPooledAmount; i ++) {
				addNewObject();
			}
		}

		/// <summary>
		/// Return a pooled object.
		/// </summary>
		public GameObject create()
		{
			// Try to get an idle object.
			int index = getAnIdleObject();

			// If can't get an idle object, return null.
			if (index < 0) return null;

			// Reset object and return it.
			m_ObjectInterfaces[index].reset();
			return m_ObjectPool[index];
		}

		/// <summary>
		/// Gets an idle object.
		/// </summary>
		/// <returns>An idle object.</returns>
		int getAnIdleObject()
		{
			if (m_ReadyToUse.Count > 0) {
				return m_ReadyToUse.Dequeue();
			}

			if (m_ObjectPool.Count < m_MaxPooledAmount
			 || m_GrowOutAble) {
				addNewObject();
				return m_ReadyToUse.Dequeue();
			}

			return -1;
		}

		/// <summary>
		/// Adds one new object in pooling list.
		/// </summary>
		void addNewObject()
		{
			GameObject poolableObject= GameObject.Instantiate(Prefab);
			IPoolableObject Interface = poolableObject.GetComponent<IPoolableObject>();
			m_ObjectPool.Add(poolableObject);
			m_ObjectInterfaces.Add(Interface);
			Interface.register(this, m_ObjectPool.Count - 1);
			Interface.recycle();
		}

		public void unregister(int id) {
			m_ReadyToUse.Enqueue(id);
		}

		public GameObject Prefab {
			get { return _Prefab; }
			set {
				if (_Prefab != null) {
					Debug.LogWarning("Trying to reset an object pool!!!");
				}

				if (value.GetComponent<IPoolableObject>() == null) {
					Debug.LogError("This prefab:\"" + value.name + "\" didn't have an IPoolableObject attached!!!");
					return;
				} 

				_Prefab = value;
			}
		}
	}
}