﻿using UnityEngine;
using System.Collections;
/*
namespace TDGame.Launcher
{
	public enum DispersionShape
	{
		Random,
	}

	public enum LauncherParameter
	{
		Name,
		ID,
		MinDisp,
		MaxDisp,
		DispInc,
		DispDec,
		HeatInc,
		HeatDec,
		HeatDownDelay,
		OverHeat,
		Range,
		Damage,
		Impulse,
		BulletSpeed,
		BulletRotate,
		BulletAmount,
		FireTimes,
		FireRate,
		Charge,
		CoolDown,
		PowerUp,
		Projectile,
	}

	[System.Serializable]
	public class LaunchData
	{
		[Tooltip("The name of this launcher spell.")]
		[SerializeField] public string Name;

		[Tooltip("The ID of this launcher spell.")]
		[SerializeField] public int ID;

		[Header("Fire Mode")]
		[Tooltip(
			"Is this spell a burst spell.\n" +
			"When this sets to true, this spell will automatic fire a full load after release the fire botton," +
			"sets to false then it will be stop by the release of fire trigger or a full load have fired.\n" +
			"You CAN'T set both burst and auto to true!!!")]
		[SerializeField] public bool BurstFire;

		[Tooltip(
			"Is this spell a auto spell.\n" +
			"When this sets to true, this spell will start next round after last round is completed." +
			"Sets to false then next round won't start until release and push again the fire trigger.\n" +
			"You CAN'T set both burst and auto to true!!!")]
		[SerializeField] public bool AutoFire;

		[Header("Dispersion")]
		[Tooltip("The shape of dispersion.")]
		[SerializeField] public DispersionShape	DispShape;

		[Range(0f, 180f)]
		[Tooltip(
			"The minimum dispersion. \n" + 
			"0 to no dispersion and 180 to full dispersion")]
		[SerializeField] public float MinDisp;

		[Range(0f, 180f)]
		[Tooltip(
			"The maximum dispersion. \n" + 
			" 0 to no dispersion and 180 to full dispersion")]
		[SerializeField] public float MaxDisp;

		[Range(0f, 2f)]
		[Tooltip(
			"Dispersion increase value. \n" + 
			" How much dispersion will add in each shoot.")]
		[SerializeField] public float DispInc;

		[Range(0f, 5f)]
		[Tooltip(
			"Dispersion decrease rate.\n" +
			" How much dispersion will depress in second.")]
		[SerializeField] public float DispDec;

		[Header("Heat")]
		[Tooltip(
			"Heat increase value. \n" + 
			" How much Heat will add in each shoot.")]
		[SerializeField] public float HeatInc;

		[Tooltip(
			"Heat decrease rate.\n" +
			" How much Heat will depress in second.")]
		[SerializeField] public float HeatDec;

		[Tooltip("How long the overheat state will last.")]
		[SerializeField] public float HeatDownDelay;

		[Tooltip("How long the overheat state will last.")]
		[SerializeField] public float OverHeat;

		[Header("Basic Setting")]
		[Tooltip("How far the bullet can fly.")]
		[SerializeField] public float Range;

		[Tooltip("How powerful this spell is!")]
		[SerializeField] public float Damage;

		[Tooltip("How powerful this spell is!(in physical way.)")]
		[SerializeField] public float Impulse;

		[Tooltip("How fast the bullet will fly.")]
		[SerializeField] public float BulletSpeed;

		[Tooltip("How the bullet rotate.")]
		[SerializeField] public Vector3 BulletRotate;

		[Tooltip("How much bullet will shoot out once.")]
		[SerializeField] public int BulletAmount;

		[Tooltip("How many times this spell can fire in a round, 0 mean it can keep fire till it being stop.")]
		[SerializeField] public int FireTimes;

		[Range(0f, 5f)]
		[Tooltip(
			"How fast this spell can fire.\n" +
			"unit is second per shoot. If less than fixedDeltatime than it will fire as fast as the game allow.")]
		[SerializeField] public float FireRate;

		[Tooltip(
			"The max charge volume, unit is second.\n" +
			" 0 means no charge.")]
		[SerializeField] public float Charge;

		[Tooltip("The charge effect to this spell.")]
		[SerializeField] public ChargeData ChargeEffects;

		[Tooltip("After a fire round, how long can start next round. unit is second.")]
		[SerializeField] public float CoolDown;

		[Tooltip("How long this spell need to prepare before shootable again.")]
		[SerializeField] public float PowerUp;

		[Tooltip("Offset of fire position.")]
		[SerializeField] public Vector3 FirePosOffset;

		[Tooltip("Offset of fire direction.")]
		[SerializeField] public Vector3 FireDirOffset;

		[Tooltip("The projectile this spell will shoot.")]
		[SerializeField] public GameObject Projectile;

		[Tooltip(
			"Is this spell calculate heat by it's own.\n" +
			"When this sets to true, this spell will calculate heat by it's own, other spell won't affect it.")]
		[SerializeField] public bool StandaloneHeat;

		[Tooltip(
			"Is this spell calculate cooldown by it's own.\n" +
			"When this sets to true, this spell will calculate cooldown by it's own, other spell won't affect it.")]
		[SerializeField] public bool StandaloneCooldown;

		//--------------------------------------------------------------------------------------

		//Default setting
		public LaunchData()
		{
			Name			= "Default";
			ID				= 0;
			BurstFire		= false;
			AutoFire		= false;
			DispShape		= DispersionShape.Random;
			MinDisp			= 0;
			MaxDisp			= 10;
			DispInc			= 0.2f;
			DispDec			= 0.4f;
			HeatInc			= 2;
			HeatDec			= 4;
			HeatDownDelay	= 1;
			OverHeat		= 3;
			Range			= 600;
			Damage			= 10;
			Impulse			= 10;
			BulletSpeed 	= 30;
			BulletRotate	= Vector3.zero;
			BulletAmount	= 1;
			FireTimes		= 1;
			FireRate		= 0.2f;
			Charge			= 0;
			CoolDown		= 0.2f;
			PowerUp			= 0.1f;
			FirePosOffset	= Vector3.zero;
			FireDirOffset	= Vector3.zero;
			Projectile		= null;
			StandaloneHeat	= false;
			StandaloneCooldown = false;
		}

		public LaunchData(LaunchData data)
		{
			Name				= data.Name;
			ID					= data.ID;
			BurstFire			= data.BurstFire;
			AutoFire			= data.AutoFire;
			DispShape			= data.DispShape;
			MinDisp				= data.MinDisp;
			MaxDisp				= data.MaxDisp;
			DispInc				= data.DispInc;
			DispDec				= data.DispDec;
			HeatInc				= data.HeatInc;
			HeatDec				= data.HeatDec;
			HeatDownDelay		= data.HeatDownDelay;
			OverHeat			= data.OverHeat;
			Range				= data.Range;
			Damage				= data.Damage;
			Impulse				= data.Impulse;
			BulletSpeed 		= data.BulletSpeed;
			BulletRotate		= data.BulletRotate;
			BulletAmount		= data.BulletAmount;
			FireTimes			= data.FireTimes;
			FireRate			= data.FireRate;
			Charge				= data.Range;
			ChargeEffects		= data.ChargeEffects;
			CoolDown			= data.CoolDown;
			PowerUp				= data.PowerUp;
			FirePosOffset 		= data.FirePosOffset;
			FireDirOffset 		= data.FireDirOffset;
			Projectile 			= data.Projectile;
			StandaloneHeat		= data.StandaloneHeat;
			StandaloneCooldown	= data.StandaloneCooldown;
		}

		public LaunchData getCopy()
		{
			return (LaunchData)this.MemberwiseClone();
		}
	}
}
*/