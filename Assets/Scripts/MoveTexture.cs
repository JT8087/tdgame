﻿using UnityEngine;
using System.Collections;

public class MoveTexture : MonoBehaviour {
	[SerializeField] float m_XSpeed = 1;
	[SerializeField] float m_YSpeed = 1;
	[SerializeField] Material m_Material = null;

	void Start() {
		if (m_Material == null) this.enabled = false;
	}

	void Update() {
		Vector2 offset = m_Material.mainTextureOffset;
		offset.x += m_XSpeed * Time.deltaTime;
		offset.y += m_YSpeed * Time.deltaTime;
		offset.x -= offset.x > 1? 1: 0;
		offset.y -= offset.y > 1? 1: 0;
		m_Material.mainTextureOffset = offset;
	}
}
