﻿using UnityEngine;
using System.Collections;

namespace TDGame.Turrets
{
	public class EffectGroup : MonoBehaviour
	{
		public Light[] lights;
		public ParticleSystem[] particles;

		void Awake()
		{
			lights = GetComponentsInChildren<Light>();
			particles = GetComponentsInChildren<ParticleSystem>();
		}

		void OnEnable()
		{
			Play();
		}

		void OnDisable()
		{
			Stop();
		}

		void Start()
		{
			this.enabled = false;
		}

		public void Play() {
			PlayLight();
			PlayParticle();
		}

		public void Stop() {
			StopLight();
			StopParticle();
		}

		public void PlayLight() {
			for (int i = 0; i < lights.Length; i ++) {
				lights[i].enabled = true;
			}
		}

		public void PlayParticle() {
			for (int i = 0; i < particles.Length; i ++) {
				particles[i].Play();
			}
		}

		public void StopLight() {
			for (int i = 0; i < lights.Length; i ++) {
				lights[i].enabled = false;
			}
		}

		public void StopParticle() {
			for (int i = 0; i < particles.Length; i ++) {
				particles[i].Stop();
			}
		}
	}
}
