﻿using UnityEngine;
using System.Collections;

public class QueueBehaviour : MonoBehaviour
{
	[SerializeField][Range(0, QueueUpdateManager.MaxQueue - 1)]protected int QueuePriority;

	void OnEnable()
	{
		QueueUpdateManager.QueueUpdateList[QueuePriority] 		+= QueueUpdate;
		QueueUpdateManager.QueueFixedUpdateList[QueuePriority] 	+= QueueFixedUpdate;
		QueueUpdateManager.QueueLateUpdateList[QueuePriority] 	+= QueueLateUpdate;
	}

	void OnDisable()
	{
		QueueUpdateManager.QueueUpdateList[QueuePriority] 		-= QueueUpdate;
		QueueUpdateManager.QueueFixedUpdateList[QueuePriority] 	-= QueueFixedUpdate;
		QueueUpdateManager.QueueLateUpdateList[QueuePriority] 	-= QueueLateUpdate;
	}
			
	protected virtual void QueueUpdate(){}
	protected virtual void QueueFixedUpdate(){}
	protected virtual void QueueLateUpdate(){}
}
