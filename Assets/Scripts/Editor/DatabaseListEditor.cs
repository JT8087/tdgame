﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(DataBaseList))]
public class DatabaseListEditor : Editor {
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
	}
}
