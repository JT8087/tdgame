﻿using UnityEngine;
using System.Collections;

namespace TDGame.Enemys
{
	public class Walker : Enemy {
		public float m_MinHealth = 100;
		public float m_MaxHealth = 1000;
		float m_DefHealth;

		void Awake()
		{
			m_DefHealth = m_Health;
		}

		void Update()
		{
			nextWayPoint();

			if (m_TargetWaypoint < WayPoints.WaypointSet.Length) {
				move();
			} else {
				reachEnd();
			}
		}

		// ----------------------------------------------------------------------
		// Internal method

		protected override void move()
		{
			Vector3 dir = WayPoints.WaypointSet[m_TargetWaypoint].position - transform.position;
			float angle = Vector3.Angle(transform.forward, dir) * Mathf.Sign(Vector3.Cross(transform.forward, dir).y);
			transform.Translate(Vector3.forward * m_Speed * Time.deltaTime);
			transform.Rotate(Vector3.up, angle * m_Speed * Time.deltaTime);
		}

		protected override void nextWayPoint()
		{
			if (Vector3.Distance(transform.position, WayPoints.WaypointSet[m_TargetWaypoint].position) < 1.5f) {
				m_TargetWaypoint ++;
			}
		}

		protected override void reachEnd()
		{
			GameManager.enemyReached(m_Damage);
			recycle();
		}

		// ----------------------------------------------------------------------
		// Public method

		public override void gotHit(float damage)
		{
			m_Health -= damage;
			if (isDead()) {
				GameManager.enemyEliminated(m_Reward);
				recycle();
			}
		}

		public override bool isDead()
		{
			return m_Health <= 0;
		}

		public override void recycle()
		{
			pool.unregister(id);
			gameObject.SetActive(false);

			// Put it far away from scene.
			Vector3 downPosition = transform.position;
			downPosition.y = -100;
			transform.position = downPosition;
		}

		public override bool isIdle()
		{
			return !gameObject.activeInHierarchy;
		}

		public override void reset()
		{
			// Test
			m_Health = Mathf.Lerp(m_MaxHealth, m_MinHealth, Random.value);
			//m_Health = m_DefHealth;
			m_TargetWaypoint = 0;
			gameObject.SetActive(true);
		}
	}
}
