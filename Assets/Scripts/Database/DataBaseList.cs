﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DataBaseList :ScriptableObject
{
	[SerializeField]
	public List<DataBaseSturt> DataList = new List<DataBaseSturt>();

	public GameObject GetData(int id)
	{
		if (DataList[id].ID == id)
			return DataList[id].Object;

		for (int i = 0; i < DataSetLength; i++) {
			if (DataList[i].ID == id)
				return DataList[i].Object;
		}
		return null;
	}

	public GameObject GetData(string name)
	{
		for (int i = 0; i < DataSetLength; i++) {
			if (DataList[i].Name.ToLower().Equals(name.ToLower()))
				return DataList[i].Object;
		}
		return null;
	}

	public int GetDataIndex(int id)
	{
		if (DataList[id].ID == id)
			return id;
		
		for (int i = 0; i < DataSetLength; i++) {
			if (DataList[i].ID == id)
				return i;
		}
		return -1;
	}

	public int GetDataIndex(string name)
	{
		for (int i = 0; i < DataSetLength; i++) {
			if (DataList[i].Name.ToLower().Equals(name.ToLower()))
				return i;
		}
		return -1;
	}

	public int getIDFromName(string name)
	{
		for (int i = 0; i < DataSetLength; i++) {
			if (DataList[i].Name.ToLower().Equals(name.ToLower()))
				return DataList[i].ID;
		}
		return -1;
	}

	public string getNameFromID(int id)
	{
		if (DataList[id].ID == id)
			return DataList[id].Name;
		
		for (int i = 0; i < DataSetLength; i++) {
			if (DataList[i].ID == id)
				return DataList[i].Name;
		}
		return "";
	}

	public void uniquifyID()
	{
		for (int i = 0; i < DataSetLength; i ++) {
			DataBaseSturt data = DataList[i];
			data.ID = i;
			DataList[i] = data;
		}
	}

	public int DataSetLength
	{
		get { return DataList.Count; }
	}

	public GameObject getObject(int index)
	{
		return DataList[index].Object;
	}
}
